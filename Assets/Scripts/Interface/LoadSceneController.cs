﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSceneController : MonoBehaviour
{
    [Header("Images")] //Cabeçalho do Inspector.
    public RawImage loadingWater;
    public RawImage loadingBackground;
    public TextMeshProUGUI loadingPortugueseText;
    public TextMeshProUGUI loadingEnglishText;
    public static ScreenOrientation ornt = ScreenOrientation.AutoRotation;
    
    public static string nextScene;

    public void Start ()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                loadingEnglishText.gameObject.SetActive(false);
                loadingPortugueseText.gameObject.SetActive(true);
                StartCoroutine(LoadPortuguese(1));
                break;
            case 1:
                loadingEnglishText.gameObject.SetActive(true);
                loadingPortugueseText.gameObject.SetActive(false);
                StartCoroutine(LoadEnglish(1));
                break;
            default:
                break;
        }

        StartCoroutine(LoadScene());

        switch (Input.deviceOrientation)
        {
            case DeviceOrientation.LandscapeRight:
                ornt = ScreenOrientation.LandscapeRight;
                break;
            case DeviceOrientation.LandscapeLeft:
                ornt = ScreenOrientation.LandscapeLeft;
                break;
            default:
                ornt = ScreenOrientation.Landscape;
                break;
        }
    }

    private IEnumerator LoadScene()
    {
        var load = SceneManager.LoadSceneAsync(nextScene);

        while (!load.isDone)
        {
            yield return new WaitForEndOfFrame();
        }
    }

    public void Update()
    {
        loadingWater.uvRect = new Rect(loadingWater.uvRect.x + (Time.deltaTime / 20), 0, 1, 1);
        loadingBackground.uvRect = new Rect(loadingBackground.uvRect.x + (Time.deltaTime / 10), 0, 1, 1);
    }

    private IEnumerator LoadPortuguese(float delay)
    {
        loadingPortugueseText.text = "Carregando<b>.</b>";
        yield return new WaitForSeconds(delay);
        loadingPortugueseText.text = "Carregando<b>..</b>";
        yield return new WaitForSeconds(delay);
        loadingPortugueseText.text = "Carregando<b>...</b>";
        yield return new WaitForSeconds(delay);
        StartCoroutine(LoadPortuguese(1));
    }

    private IEnumerator LoadEnglish(float delay)
    {
        loadingEnglishText.text = "Loading<b>.</b>";
        yield return new WaitForSeconds(delay);
        loadingEnglishText.text = "Loading<b>..</b>";
        yield return new WaitForSeconds(delay);
        loadingEnglishText.text = "Loading<b>...</b>";
        yield return new WaitForSeconds(delay);
        StartCoroutine(LoadEnglish(1));
    }
}
