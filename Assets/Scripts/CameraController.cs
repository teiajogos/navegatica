﻿using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.UI;

//Classe que define as configurações e comportamento da câmera do jogo.
public class CameraController : MonoBehaviour
{
    [Header("References")] //Cabeçalho do Inspector.
    public PlayerController playerController;
    public GameObject player; //Referência do game object do jogador.
    public RawImage background; //Referência do game object do jogador.

    [Header("Border Limits of the Camera")] //Cabeçalho do Inspector.
	public float minX; //Limite mínimo no eixo X da câmera.
	public float maxX; //Limite máximo no eixo X da câmera.

	[Header("Camera Properties")] //Cabeçalho do Inspector.
	public float dampTime; //Tempo que a câmera demora para ficar na mesma posição do jogador.

	[HideInInspector] //Não mostra no Inspector.
	public float cameraHeight, cameraWidth; //Variáveis que guardam a altura e largura da câmera.

	[HideInInspector] //Não mostra no Inspector.
	public bool cameraIsMoving = false; //Variável que testará se a câmera está ou não seguindo o barco.

	private Vector3 _offset; //Distância da câmera para o jogador.
	private Vector3 _velocity = Vector3.zero; //Velocidade da interpolação será zero em todos os eixos.

	//Start é chamado antes do Update.
	private void Start()
	{
        cameraHeight = 2f * Camera.main.orthographicSize; //Variável que guarda a altura da câmera de acordo com seu tamanho ortográfico.
		cameraWidth = cameraHeight * Camera.main.aspect; //Variável que guarda a largura da câmera de acordo com seu tamanho ortográfico.

		GameObject lastObject = GameObject.FindGameObjectWithTag("Quay"); //Game object que guarda um objeto que tenha a tag entre parênteses.

		maxX = lastObject.transform.position.x + lastObject.transform.localScale.x - (cameraWidth / 2.3f); //O limite máximo no eixo X da câmera será a posição X do último bloco criado + a metade de seua largura - a metade da largura da câmera para que a mesma trave ao atingir o canto direito do objeto.

		_offset = transform.position - player.transform.position; //Calcula e guarda a distância entre o jogador e a câmera.
	}

	//LateUpdate é chamado após Update em cada frame.
	private void LateUpdate()
	{
	    if (!(player.transform.position.x > minX) || !(player.transform.position.x < maxX)) return;

	    Vector3 desiredPosition = new Vector3(player.transform.position.x, transform.position.y) + _offset; //A posição deseja da câmera acompanha a posição do jogador em X, mantém seu Y e respeita a distância estabelecida por offset.
	    Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref _velocity, dampTime); //A posição suavizada da câmera será uma interpolação entre a posição da cãmera e a posição desejada sem alterações nos eixos utilizando a velocidade estabelecida em dampTime.
	    transform.position = smoothedPosition; //A posição da câmera recebe sua posição suavizada.

	    //if (playerController.playerIsMoving) //Se o navio estiver a câmera estiverem se movendo: 
        //{
        //    offset += Time.deltaTime * speed; //Offset muda de acordo a velocidade aplicada em ShipController.

        //    _spriteRenderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0)); //A posição X da textura se move de acordo offset.
        //}

        //else if (playerController.accel.isOn)
        //{
        //    offset += Time.deltaTime * (speed / 3);

        //    _spriteRenderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        //}
    }
}
