﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseButton : MonoBehaviour
{
    private Button button;
    public ShopMenu shopMenu;
    public ShopItem shopItem;
	public TextMeshProUGUI purchaseText;
    public TextMeshProUGUI costText;
    public GameObject selectedImage;
    public Sprite classicShip;
    public int index;
    public Texture waterTexture;
    public Texture dockTexture;
    public RawImage buttonImage;
    public static int waveIndex;

    private void Awake()
    {
        this.button = this.GetComponent<Button>();
        ShopMenu.isBought[0] = true;
    }

    private void OnEnable()
    {
        purchaseText.text = ShopMenu.buyText;
        costText.text = shopItem.cost.ToString();

        if (GameController.shipSelected == null)
        {
            GameController.shipSelected = classicShip;
        }        
    }
    
    private void Update()
    {
        VerifyIfBought();
    }

    private void VerifyIfBought()
    {
        if (!ShopMenu.isBought[index])
        {
            buttonImage.texture = dockTexture;

            if (GameController.playerScore < shopItem.cost)
            {
                button.interactable = false;
                costText.color = Color.red;
            }

            else if (GameController.playerScore >= shopItem.cost)
            {
                if (shopItem.inGameShipSprite == GameController.shipSelected)
                {
                    button.interactable = false;
                }
                else if (shopItem.inGameShipSprite != GameController.shipSelected)
                {
                    button.interactable = true;
                    costText.color = Color.black;
                }
            }
            costText.gameObject.SetActive(true);
        }

        else if (ShopMenu.isBought[index])
        {
            buttonImage.texture = waterTexture;

            if (shopItem.inGameShipSprite == GameController.shipSelected)
            {
                buttonImage.uvRect = new Rect(buttonImage.uvRect.x + (Time.deltaTime / 15), 0, 1, 1);
                button.interactable = false;
                selectedImage.gameObject.SetActive(true);
                purchaseText.gameObject.SetActive(false);
                costText.gameObject.SetActive(false);
            }
            else if (shopItem.inGameShipSprite != GameController.shipSelected)
            {
                button.interactable = true;
                selectedImage.gameObject.SetActive(false);
                purchaseText.gameObject.SetActive(true);
                purchaseText.text = ShopMenu.selectText;
            }

            costText.gameObject.SetActive(false);
        }
    }

    public void OnClick(int index)
    {
        switch (ShopMenu.isBought[index])
        {
            case false:
                PurchaseShip(index);
                break;
            case true:
                SelectShip(index);
                break;
            default:
                break;
        }
    }

    public void PurchaseShip(int index)
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);
        
        ShopMenu.isBought[index] = true;
        shopMenu.SaveBoughtShipInfo();
        
        GameController.playerScore = GameController.playerScore - shopItem.cost;

        switch (lng)
        {
            case 0:
                shopMenu.pointsText.text = "SEUS PONTOS: " + GameController.playerScore.ToString();
                break;
            case 1:
                shopMenu.pointsText.text = "YOUR POINTS: " + GameController.playerScore.ToString();
                break;
            default:
                break;
        }
        
        purchaseText.text = ShopMenu.selectText;
        costText.gameObject.SetActive(false);
    }

    public void SelectShip(int index)
    {
        button.interactable = false;
        selectedImage.gameObject.SetActive(true);
        GameController.shipSelected = shopItem.inGameShipSprite;
        GameController.shipSelectedIndex = index;
        waveIndex = index;
    }
}
