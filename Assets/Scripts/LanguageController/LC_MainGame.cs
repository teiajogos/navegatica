﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LC_MainGame : MonoBehaviour
{
    [Header("Question References")]
    public TextMeshProUGUI readQuestionButton;
    public TextMeshProUGUI guideBoatQ;
    public TextMeshProUGUI guideBoatRQ;
    public TextMeshProUGUI confirmButton;
    public TextMeshProUGUI continueButton;

    [Header("Pause Panel References")]
    public TextMeshProUGUI pausedTitle;
    public TextMeshProUGUI pauseMainMenu;
    public TextMeshProUGUI pauseStageSelection;
    public TextMeshProUGUI pauseRestart;
    public TextMeshProUGUI pauseResume;

    [Header("Defeat Panel References")]
    public TextMeshProUGUI defeatTitle;
    public TextMeshProUGUI defeatMainMenu;
    public TextMeshProUGUI defeatStageSelection;
    public TextMeshProUGUI defeatRestart;

    [Header("Game Over Panel References")]
    public TextMeshProUGUI gameOverTitle;
    public TextMeshProUGUI gameOverSubtitle;
    public TextMeshProUGUI gameOverMainMenu;
    public TextMeshProUGUI gameOverStageSelection;
    public TextMeshProUGUI gameOverRestart;

    [Header("Victory Panel References")]
    public TextMeshProUGUI victoryTitle;
    public TextMeshProUGUI victoryMainMenu;
    public TextMeshProUGUI victoryStageSelection;
    public TextMeshProUGUI victoryRestart;
    public TextMeshProUGUI victoryNext;

    [Header("Last Level Victory Panel References")]
    public TextMeshProUGUI lastVictoryTitle;
    public TextMeshProUGUI lastVictoryMainMenu;
    public TextMeshProUGUI lastVictoryStageSelection;
    public TextMeshProUGUI lastVictoryRestart;

    public void Awake()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    public void OnLanguageChanged(int languageOption)
    {
        switch (languageOption)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    private void Portuguese()
    {
        readQuestionButton.text = "Reler Pergunta";
        guideBoatQ.text = "Leve o barco até a doca correspondente á resposta!";
        guideBoatRQ.text = "Leve o barco até a doca correspondente á resposta!";
        confirmButton.text = "Começar!";
        continueButton.text = "Continuar";

        pausedTitle.text = "Jogo Pausado";
        pauseMainMenu.text = "Menu Principal";
        pauseStageSelection.text = "Seleção de Fase";
        pauseRestart.text = "Reiniciar";
        pauseResume.text = "Continuar";

        defeatTitle.text = "Resposta errada!";
        defeatMainMenu.text = "Menu Principal";
        defeatStageSelection.text = "Seleção de Fase";
        defeatRestart.text = "Reiniciar";

        gameOverTitle.text = "Fim de Jogo";
        gameOverSubtitle.text = "O tempo acabou!";
        gameOverMainMenu.text = "Menu Principal";
        gameOverStageSelection.text = "Seleção de Fase";
        gameOverRestart.text = "Reiniciar";

        victoryTitle.text = "Resposta correta!";
        victoryMainMenu.text = "Menu Principal";
        victoryStageSelection.text = "Seleção de Fase";
        victoryRestart.text = "Reiniciar";
        victoryNext.text = "Próxima Questão";

        lastVictoryTitle.text = "Nível Concluído";
        lastVictoryMainMenu.text = "Menu Principal";
        lastVictoryStageSelection.text = "Seleção de Fase";
        lastVictoryRestart.text = "Reiniciar";
    }

    private void English()
    {
        readQuestionButton.text = "Read Question";
        guideBoatQ.text = "Guide the boat to the dock with the correct answer!";
        guideBoatRQ.text = "Guide the boat to the dock with the correct answer!";
        confirmButton.text = "Start!";
        continueButton.text = "Continue";

        pausedTitle.text = "Paused Game";
        pauseMainMenu.text = "Main Menu";
        pauseStageSelection.text = "Level Selection";
        pauseRestart.text = "Restart";
        pauseResume.text = "Resume";

        defeatTitle.text = "Wrong answer!";
        defeatMainMenu.text = "Main Menu";
        defeatStageSelection.text = "Level Selection";
        defeatRestart.text = "Restart";

        gameOverTitle.text = "Game Over";
        gameOverSubtitle.text = "Time's up!";
        gameOverMainMenu.text = "Main Menu";
        gameOverStageSelection.text = "Level Selection";
        gameOverRestart.text = "Restart";

        victoryTitle.text = "Correct answer!";
        victoryMainMenu.text = "Main Menu";
        victoryStageSelection.text = "Level Selection";
        victoryRestart.text = "Restart";
        victoryNext.text = "Next Question";

        lastVictoryTitle.text = "Level complete!";
        lastVictoryMainMenu.text = "Main Menu";
        lastVictoryStageSelection.text = "Level Selection";
        lastVictoryRestart.text = "Restart";
    }
}
