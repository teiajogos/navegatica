﻿using System.Collections.Generic; //Utilizado para acessar as listas.
using System.Xml; //Utilizado para trabalhar com arquivos XML.
using System.Xml.Serialization; //Utilizado para ler e gravar informações em arquivos XML.

//Classe de disciplinas.
[System.Serializable] //Permite que a classe possa ser acessada no Inspector.
public class Subject
{
	[XmlAttribute("name")] //Aparecerá no arquivo XML como na string.
	public string name; //Nome da disciplina

	[XmlArray("Grades"), XmlArrayItem("Grade")] //Aparecerá no arquivo XML como na string.
	public List<Grade> grades; //Lista derivada da classe Grade com todas as séries/anos.
}

//Classe de séries.
[System.Serializable] //Permite que a classe possa ser acessada no Inspector.
public class Grade
{
	[XmlAttribute("index")] //Aparecerá no arquivo XML como na string.
	public int index; //Variável que abriga o índice da série/ano atual (começa em 1).

	[XmlArray("Questions"), XmlArrayItem("Question")] //Aparecerá no arquivo XML como na string.
	public List<Question> questions; //Lista derivada da classe Question com todas as questões respectivas à série/ano atual.
}

//Classe de questões.
[System.Serializable] //Permite que a classe possa ser acessada no Inspector.
public class Question
{
	[XmlAttribute("index")] //Aparecerá no arquivo XML como na string.
	public int index; //Variável que abriga o índice da questão atual (começa em 1).

	[XmlAttribute("question")] //Aparecerá no arquivo XML como na string.
	public string question; //Variável que abriga a questão atual.

	[XmlArray("Answers"), XmlArrayItem("Answer")] //Aparecerá no arquivo XML como na string.
	public List<string> answers = new List<string>(4); //Lista de strings com todas as respostas respectivas à questão atual.

	[XmlAttribute("letter")] //Aparecerá no arquivo XML como na string.
	public string letter; // Letra correspondente à resposta correta.
}