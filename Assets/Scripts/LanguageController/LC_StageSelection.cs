﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LC_StageSelection : MonoBehaviour
{
    [Header("Button References")]
    public TextMeshProUGUI backButton;

    public void Awake()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    public void OnLanguageChanged(int languageOption)
    {
        switch (languageOption)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    private void Portuguese()
    {
        backButton.text = "Voltar";
    }

    private void English()
    {
        backButton.text = "Back";
    }
}
