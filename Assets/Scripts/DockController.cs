﻿using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.

//Classe que serve para definir a posição e criação dos cais/docas de respostas.
public class DockController : MonoBehaviour
{
	[Header("References")] //Cabeçalho do Inspector.
	public TileController tileController; //Referência do TileController.
	public GameObject quaysPrefab; //Prefab que armazena os cais de respostas.

	public Vector2 _quaysPos; //A posição em que os cais serão criados.

	//Start é chamado antes do Update.
	private void Start()
	{
		_quaysPos.x = tileController.lastCreatedTile.transform.position.x + (tileController.tilePosX * 2); //A posição X em que os cais serão criados.

		GameObject newInstance = Instantiate(quaysPrefab, _quaysPos, Quaternion.identity) as GameObject; //Instancia o prefab com os blocos já configurados na posição columnPos e zera sua rotação.
		newInstance.name = "Quays"; //Define o nome do game object para melhor visualização na Hierarchy.
	}
}
