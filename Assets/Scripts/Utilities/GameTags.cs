﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameTags
{
    // Cenas
    public static string SCN_Teia = "SCN_Teia";
    public static string SCN_MainMenu= "SCN_MainMenu";
    public static string SCN_StageSelection = "SCN_StageSelection";
    public static string SCN_Loading = "SCN_Loading";
    public static string SCN_MainGame = "SCN_MainGame";

    // PlayerPrefs
    public static string PLP_Accelerometer = "Accelerometer";
    public static string PLP_ShopItem = "ShopItem";
    public static string PLP_MasterVolume = "MasterVolume";
    public static string PLP_GraphicSettings = "GraphicOption";
    public static string PLP_LanguageSettings = "LanguageOption";
}
