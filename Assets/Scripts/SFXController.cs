﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    public AudioSource source;

    public AudioClip buttonPressed;
    public AudioClip collision;
    public AudioClip collectable;
    public AudioClip wrongAnswer;
    public AudioClip correctAnswer;

    public void Start()
    {
        var src = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);
        source.volume = src * 2.5f;
    }

    public void OnButtonPressed()
    {
        source.clip = buttonPressed;
        source.Play();
    }

    public float OnButtonPressedThroughScenes()
    {
        source.clip = buttonPressed;
        source.Play();
        return buttonPressed.length;
    }

    public void OnCollision()
    {
        source.clip = collision;
        source.Play();
	}

    public void OnCollectable()
    {
        source.clip = collectable;
        source.Play();
    }

    public void OnWrongAnswer()
    {
        source.clip = wrongAnswer;
        source.Play();
    }

    public void OnCorrectAnswer()
    {
        source.clip = correctAnswer;
        source.Play();
    }
}
