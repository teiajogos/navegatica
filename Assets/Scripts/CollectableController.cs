﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour
{
    public GameObject collectableStar;
    private GameObject instance;
    private Vector3 spawn;

    public void InstantiateStar(Vector2 emptyBlockerPos)
    {
        spawn = new Vector3(emptyBlockerPos.x, emptyBlockerPos.y, 0);
        instance = Instantiate(collectableStar, spawn, new Quaternion(0, 0, 0, 0));
    }

    private void Update()
    {
        if (instance != null)
        {
            instance.transform.position = spawn;
        }
    }
}
