﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSwirlController : MonoBehaviour
{
    public GameObject swirl;
    private GameObject instance;
    private Vector3 spawn;

    public void InstantiateSwirl(Vector2 emptyBlockerPos)
    {
        spawn = new Vector3(emptyBlockerPos.x, emptyBlockerPos.y, 0);
        instance = Instantiate(swirl, spawn, new Quaternion(0, 0, 0, 0));
    }

    //private void Update()
    //{
    //    if (instance != null)
    //    {
    //        instance.transform.position = spawn;
    //    }
    //}
}