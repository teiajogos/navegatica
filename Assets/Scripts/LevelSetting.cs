﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSetting : MonoBehaviour
{
    public List<Levels> gameLevels = new List<Levels>();
}

[System.Serializable]
public class Levels
{
    public string name;

    [Header("Obstacles")]
    public int shipAmount;
    public int bridgeAmount;
    public int swirlAmount;

    [Header("Collectables")]
    public int starAmount;
}
