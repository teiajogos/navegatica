﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LC_Teia : MonoBehaviour
{
    [Header("Button References")]
    public TextMeshProUGUI loginButton;
    public TextMeshProUGUI guestButton;
    public TextMeshProUGUI incorrectPassword;
    public TextMeshProUGUI noConnection;

    public void Awake()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    public void OnLanguageChanged(int languageOption)
    {
        switch (languageOption)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    private void Portuguese()
    {
        loginButton.text = "Entrar";
        guestButton.text = "Visitante";
        incorrectPassword.text = "E-mail ou senha inválidos.";
        noConnection.text = "Conexão à internet indisponível.";
    }

    private void English()
    {
        loginButton.text = "Login";
        guestButton.text = "Guest";
        incorrectPassword.text = "Invalid e-mail or password.";
        noConnection.text = "Internet connection unavailable.";
    }
}
