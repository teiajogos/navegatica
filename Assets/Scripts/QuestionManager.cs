﻿using System.Collections.Generic; //Utilizado para trabalhar com listas.
using System.Xml.Serialization; //Utilizado para ler e gravar informações em arquivos XML.
using System.IO; //Utilizado para ler e gravar arquivos e dados.
using TMPro; //Utilizado para trabalhar com o plugin TextMeshPro.
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using V_Lib;

//Classe que gerencia o sistema de questões do jogo.
public class QuestionManager : MonoBehaviour
{
	[Header("Script References")] //Cabeçalho do Inspector.
	public GameController gameController; //Referência de GameController.

	[Header("Text References")] //Cabeçalho do Inspector.
	public List<TextMeshProUGUI> questionText; //Texto que contém a questão.
	public List<TextMeshProUGUI> answerAText; //Texto que contém a alternativa A.
	public List<TextMeshProUGUI> answerBText; //Texto que contém a alternativa B.
	public List<TextMeshProUGUI> answerCText; //Texto que contém a alternativa C.
	public List<TextMeshProUGUI> answerDText; //Texto que contém a alternativa D.

	[HideInInspector] //Não mostra no Inspector.
	public List<Subject> subjects; //Lista que abrigará todas as disciplinas.
	private Question _currentQuestion; //Questão atual.

    public static List<Subject> LoadXMLQuiz()
    {
        var xmlFile = Resources.Load<TextAsset>("Data/" + "XML_Quiz");

        List<Subject> grids;

        var serializer = new XmlSerializer(typeof(List<Subject>));

        using (var reader = new System.IO.StringReader(xmlFile.text))
        {
            grids = serializer.Deserialize(reader) as List<Subject>;
        }

        return grids;
    }

    //SaveData salva um arquivo no caminho especificado no parâmetro path.
    public void SaveData(string path)
	{
        XmlHelper.CreateXML(Application.dataPath + "/Resources/Data/", "XML_Quiz.xml", subjects.XmlSerialize());
    }

	//LoadData abre um arquivo no caminho especificado no parâmetro path e retorna a lista de disciplinas.
	public static List<Subject> LoadData(string path)
	{
        var xml = XmlHelper.LoadXML(path);

        List<Subject> obj = new List<Subject>();

        if (!string.IsNullOrEmpty(xml))
        {
            obj = xml.XmlDeserialize<List<Subject>>();
        }

        return obj;
    }

    private void Awake()
    {
        subjects = LoadXMLQuiz();
    }

    //Start é chamado antes do Update.
    private void Start()
	{
		SetCurrentQuestion(); //Chama a função que define qual é questão atual.
	}

	//Define qual é questão atual.
	private void SetCurrentQuestion()
	{
		_currentQuestion = subjects[GameController.subjectSelected].grades[GameController.gradeSelected].questions[GameController.questionSelected]; //A questão atual é definida pelo que foi selecionado na seleção de fases: disclipina, série e questão.

        foreach (var item in questionText)
        {
            item.text = _currentQuestion.question; //O texto da questão recebe a questão atual.
        }

        foreach (var item in answerAText)
        {
            item.text = _currentQuestion.answers[0]; //O texto da alternativa A recebe a alternativa A gravado na lista de respostas da questão atual.
        }

        foreach (var item in answerBText)
        {
            item.text = _currentQuestion.answers[1]; //O texto da alternativa B recebe a alternativa B gravado na lista de respostas da questão atual.
        }

        foreach (var item in answerCText)
        {
            item.text = _currentQuestion.answers[2]; //O texto da alternativa C recebe a alternativa C gravado na lista de respostas da questão atual.
        }

        foreach (var item in answerDText)
        {
            item.text = _currentQuestion.answers[3]; //O texto da alternativa D recebe a alternativa D gravado na lista de respostas da questão atual.
        }
	}

	//Função que decide se o jogador acertou ou não a questão atual, passando a letra da resposta como parâmetro.
	public void PlayerAnswer(string letter)
	{
        int qtdQuestion = (subjects[GameController.subjectSelected].grades[GameController.gradeSelected].questions.Count) - 1;
        
        if (letter == _currentQuestion.letter && GameController.questionSelected != qtdQuestion) //Se a letra é igual à resposta correta da questão:
		{
			gameController.ActivateScreen(gameController.victory); //Ativa a tela de vitória do jogo.
            //_highscore = subjects[GameController.subjectSelected].grades[GameController.gradeSelected].questions[GameController.questionSelected].highscore[GameController.questionHighScore];
        }
        else if (letter == _currentQuestion.letter && GameController.questionSelected == qtdQuestion) //Se a letra é igual à resposta correta da questão, e a questão é a última do nível atual:
        {
            gameController.ActivateScreen(gameController.lastQuestion); //Ativa a tela de vitória na última questão do jogo.
        }
        else if (letter != _currentQuestion.letter) //Se for diferente:
        {
			gameController.ActivateScreen(gameController.defeat); //Ativa a tela de derrota do jogo.
		}
	}
}