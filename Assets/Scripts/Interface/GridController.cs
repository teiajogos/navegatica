﻿using System;
using System.Collections;
using TMPro; //Utilizado para trabalhar com o plugin TextMeshPro.
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.SceneManagement; //Utilizado para tabalhar com o sistema de cenas da Unity.

//Classe que abriga as referências das páginas que utilizam grades (telas de seleção) e a função para o botão voltar.
public class GridController : MonoBehaviour
{
    [Header("Grid References")]
    public TextMeshProUGUI titleText; //Título da página.
	public GameObject subjectsGrid; //Game object com a grade de disciplinas.
	public GameObject gradesGrid; //Game object com a grade de séries.
	public GameObject questionsGrid; //Game object com a grade de questões.
    public int executionOrder; //Variável que diz qual é a ordem de execução de cada página.

    [Header("Other References")]
    public SFXController sfxController;

    //Função para voltar entre as páginas (botão "Voltar").
    public void GoBack()
	{
		switch (executionOrder) //Seleciona executionOrder e verifica:
		{
			case 1: //Caso 1:
				subjectsGrid.SetActive(false); //Desativa a grade de disciplinas.
                ReturnToMenu(); //Vai para a cena do menu principal.
				break; //Quebra do switch.
			case 2: //Caso 2:
				gradesGrid.SetActive(false); //Desativa a grade de séries.
				subjectsGrid.SetActive(true); //Ativa a grade de disciplinas.
				break; //Quebra do switch.
			case 3: //Caso 3:
				questionsGrid.SetActive(false); //Desativa a grade de questões.
				gradesGrid.SetActive(true); //Ativa a grade de séries.
				break; //Quebra do switch.
			default: //Se não existir nada em executionOrder:
				break; //Quebra do switch.
		}
	}

    public void ReturnToMenu()
    {
        var soundLength = sfxController.OnButtonPressedThroughScenes();
        StartCoroutine(WaitForButtonClick(soundLength));
    }

    private IEnumerator WaitForButtonClick(float length)
    {
        while (length >= 0)
        {
            length -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(GameTags.SCN_MainMenu);
    }
}


