﻿using UnityEngine; 

public class BackgroundScroller : MonoBehaviour
{
	/*[Header("References")] //Cabeçalho do Inspector.
	public CameraController cameraController; //Referência do CameraController.
	public PlayerController playerController; //Referência do ShipController.

	[HideInInspector] //Não mostra no Inspector.
	public float speed, offset; //Variáveis que guardam a velocidade de movimentação e a posição em X da textura.

	private SpriteRenderer _spriteRenderer; //Referência do Sprite Renderer do game object.

	//Start é chamado antes do Update.
	private void Start()
	{
		_spriteRenderer = GetComponent<SpriteRenderer>(); //Pega o Sprite Renderer atrelado ao game object.

		_spriteRenderer.enabled = true; //Ativa o Sprite Renderer do objeto.

		SpriteSizeToCameraSize(); //Chama a função que transforma o sprite no tamanho da câmera.
	}

	//LateUpdate é chamado após Update em cada frame.
	private void LateUpdate()
	{
		//transform.position = new Vector2(Camera.main.transform.position.x, transform.position.y); //A posição do sprite segue a posição da câmera.

        
		if (playerController.playerIsMoving) //Se o navio estiver a câmera estiverem se movendo: 
		{
			offset += Time.deltaTime * speed; //Offset muda de acordo a velocidade aplicada em ShipController.

			_spriteRenderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0)); //A posição X da textura se move de acordo offset.
		}

        else if (playerController.accel.isOn)
        {
            offset += Time.deltaTime * (speed / 3) ;

            _spriteRenderer.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
        }
        
    }

    //Transforma o tamanho da imagem no tamanho da câmera.
    private void SpriteSizeToCameraSize()
	{
        //float spriteWidth = _spriteRenderer.sprite.bounds.size.x; //Variável que guarda a largura do sprite.
		//float spriteHeight = _spriteRenderer.sprite.bounds.size.y; //Variável que guarda a altura do sprite.

		//transform.localScale = new Vector3(25, cameraController.cameraHeight / spriteHeight, 1f); //Transforma o tamanho do sprite de acordo com o tamanho da câmera.
        //transform.localScale = new Vector3(cameraController.cameraWidth / spriteWidth, cameraController.cameraHeight / spriteHeight, 1f);
	}
    */
}
