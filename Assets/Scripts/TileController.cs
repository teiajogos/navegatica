﻿using System;
using System.Collections.Generic;
using System.Linq;
//Utilizado para trabalhar com listas.
using UnityEngine;
using Random = UnityEngine.Random;

//Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.

//Classe que configura e controla a geração procedural dos blocos do cenário.
public class TileController : MonoBehaviour
{
    public LevelSetting _levelSetting;
    public ObstacleBridgeController obstacleBridgeController;
    public ObstacleShipController obstacleShipController;
    public CollectableController collectableController;
    public ObstacleSwirlController obstacleSwirlController;

    [Header("References")] //Cabeçalho do Inspector.
    public GameObject[] tilesPositions; //Array que armazena os blocos dentro do prefab.
    public List<GameObject> blockerSets;

    [Header("Grid Properties")] //Cabeçalho do Inspector.
    public int numRows; //Número de linhas da grade de blocos.
    public int numColumns;  //Número de colunas da grade de blocos.
    public float tileSize; //Tamanho em unidades de cada bloco.
    public float tilePosX; //Posição X da primeira coluna de blocos.

    [Header("Hierarchy Properties")] //Cabeçalho do Inspector.
    public string tilesName; //Nome do grupo de blocos na Hierarchy.
    public string columnName; //Nome das colunas na Hierarchy.
    public string rowName; //Nome das linhas na Hierarchy.

    [HideInInspector] //Não mostra no Inspector.
    public GameObject lastCreatedTile; //Variável guarda a última coluna que será instanciada.

    private List<GameObject> blockers; //Array que armazena os sprites que servem como bloqueadores/colisores.
    private Vector2 _columnPos; //Variável que armazenará a posição da coluna a ser criada.
    private int _emptyIndex; //Variável que armazenará o índice da próxima posição vazia da grade.
    private int _lastPos; //Variável que armazenará a última posição vazia da grade.
    private GameObject _lastColumn; //Game object que armazena a última coluna gerada.

    private int blockerSetAmount;
    private Dictionary<int, Vector2> obstacleShip;
    private Dictionary<int, Vector2> obstacleBridge;
    private Dictionary<int, Vector2> collectable;
    private Dictionary<int, Vector2> obstacleSwirl;

    //Start é chamado antes do Update.
    private void Start()
    {
        //numColumns = 10 + (GameController.questionSelected * (GameController.gradeSelected + 1));
        numColumns = 10 + GameController.questionSelected;
        _columnPos.x = tilePosX; //A posição X de _columnPos recebe a posição definida no Inspector.
        _emptyIndex = Random.Range(0, numRows - 1); //A primeira posição vazia da grade é randomizada de 0 até o número de linhas - 1.
        _lastPos = _emptyIndex; //A última posição vazia será emptyIndex.
        
        obstacleShip = new Dictionary<int, Vector2>();
        obstacleBridge = new Dictionary<int, Vector2>();
        collectable = new Dictionary<int, Vector2>();
        obstacleSwirl = new Dictionary<int, Vector2>();

        blockers = new List<GameObject>();

        blockerSetAmount = Random.Range(0, 3);

        for (var i = 0; i < blockerSets[blockerSetAmount].transform.childCount; i++)
        {
            blockers.Add(blockerSets[blockerSetAmount].transform.GetChild(i).gameObject);
        }

        DefineSpecialObstacles();
        
        DefineObstacles();
    }

    private void DefineSpecialObstacles()
    {
        var qst = GameController.questionSelected;

        var ships = _levelSetting.gameLevels[qst].shipAmount;
        var bridges = _levelSetting.gameLevels[qst].bridgeAmount;
        var swirls = _levelSetting.gameLevels[qst].swirlAmount;
        var stars = _levelSetting.gameLevels[qst].starAmount;

        var colunas = new List<int>();

        for (var i = 0; i < numColumns; i++)
        {
            colunas.Add(i);
        }

        for (var a = 0; a < ships; a++)
        {
            var w = Random.Range(1, colunas.Count - 1);
            
            obstacleShip.Add(colunas[w], Vector2.zero);
            colunas.RemoveAt(w);
        }

        for (var b = 0; b < bridges; b++)
        {
            var x = Random.Range(1, colunas.Count - 1);
            
            obstacleBridge.Add(colunas[x], Vector2.zero);
            colunas.RemoveAt(x);
        }

        for (var c = 0; c < swirls; c++)
        {
            var y = Random.Range(1, colunas.Count - 1);
            
            obstacleSwirl.Add(colunas[y], Vector2.zero); 
            colunas.RemoveAt(y);
        }
        
        for (var c = 0; c < stars; c++)
        {
            var z = Random.Range(1, colunas.Count - 1);
            
            collectable.Add(colunas[z], Vector2.zero);
            colunas.RemoveAt(z);
        }
    }

    private void DefineObstacles()
    {
        GameObject tilesParent = new GameObject {name = tilesName}; //Cria o objeto pai que abrigará os blocos.

        for (var i = 0; i < numColumns; i++) //Loop é repetido de acordo com o número de colunas definido pelo usuário.
        {
            _columnPos += new Vector2(tileSize, 0); //A posição da coluna a ser criada é a posição da última mais o tamanho de cada bloco para que tenha o encaixe correto.

            _lastColumn = new GameObject(); //Instancia o prefab com os blocos já configurados na posição columnPos e zera sua rotação.
            _lastColumn.transform.SetParent(tilesParent.transform);
            _lastColumn.name = columnName + (i + 1).ToString(); //Define o nome do game object de acordo com seu índice + 1 para melhor visualização na Hierarchy.

            if (obstacleShip.ContainsKey(i))
            {
                obstacleShip[i] = _columnPos;
            }

            else if (obstacleBridge.ContainsKey(i))
            {
                obstacleBridge[i] = _columnPos;
            }

            //else if (collectable.ContainsKey(i))
            //{
            //    collectable[i] = new Vector2(_columnPos.x, 0);
            //}

            //else if (obstacleSwirl.ContainsKey(i))
            //{
            //    obstacleSwirl[i] = new Vector2(_columnPos.x, 0);
            //}

            GetPath(i); //Chama a função que pega os dados do caminho a ser criado utilizando a coluna atual do loop.
        }
    }

    //GetPath define o caminho que será traçado utilizando o índice da coluna como parâmetro.
    private void GetPath(int actualIndex)
    {
        List<int> emptyPositions = new List<int>(); //Lista armazenará as posições que ficarão vazias na grade (o caminho a ser percorrido).

        if (actualIndex % 2 != 0) //Se o índice da coluna atual for ímpar:
        {
            _emptyIndex = UnityEngine.Random.Range(0, numRows - 1); //A próxima posição vazia da grade é randomizada de 0 até o número de linhas - 1.

            if (_lastPos < _emptyIndex) //Se a última posição vazia for menor que a gerada agora:
            {
                for (var i = _lastPos; i <= _emptyIndex; i++) //Loop é repetido da última posição até a gerada agora para pegar todos os blocos entre os dois e deixar todos vazios. 
                {
                    emptyPositions.Add(i); //Adiciona o índice às posições que ficarão vazias.
                }
            }
            else //Senão:
            {
                for (var i = _emptyIndex; i <= _lastPos; i++) //Loop é repetido da gerada agora até a última posição para pegar todos os blocos entre os dois e deixar todos vazios.
                {
                    emptyPositions.Add(i); //Adiciona o índice às posições que ficarão vazias.
                }
            }
        }
        else //Se o índice da coluna atual for par ou zero:
        {
            emptyPositions.Add(_lastPos); //Adiciona a última posição vazia às posições que ficarão vazias.
        }

        _lastPos = _emptyIndex; //A última posição vazia recebe emptyIndex.

        if (!obstacleShip.ContainsKey(actualIndex) && !obstacleBridge.ContainsKey(actualIndex))
        {
            SetPath(emptyPositions, collectable.ContainsKey(actualIndex), obstacleSwirl.ContainsKey(actualIndex));
        }

        else if (obstacleShip.ContainsKey(actualIndex))
        {
            obstacleShipController.InstantiateShip(obstacleShip[actualIndex]);
        }

        else if (obstacleBridge.ContainsKey(actualIndex))
        {
            obstacleBridgeController.InstantiateBridge(obstacleBridge[actualIndex]); 
        }

        //Chama a função que define as posições que receberão os sprites de bloqueadores e as que ficam vazias, passando como parâmetro a lista de posições vazias.
    }

    //SetPath verifica quais blocos são bloqueadores e quais são vazios e aplica ou não sprites e colisores a eles.
    public void SetPath(List<int> emptyPositions, bool _collectableCol, bool _swirlCol)
    {
        for (var i = 0; i < tilesPositions.Length; i++) //Loop percorre os blocos de cima a baixo.
        {
            GameObject randomBlocker = blockers[Random.Range(0, blockers.Count)]; //Randomiza um objeto no array de bloqueadores.

            Vector2 randomBlockerPosition = new Vector2(_columnPos.x, tilesPositions[i].transform.position.y); //Define a posição X desse bloco como a posição X da coluna atual e a posição Y de acordo com a posição Y do array tilesPositions.

            tilesPositions[i] = Instantiate(randomBlocker, randomBlockerPosition, Quaternion.identity, _lastColumn.transform); //Instancia o bloco randomizado na posição randomBlockerPosition tendo _lastColumn como pai. 
            tilesPositions[i].name = rowName + (i + 1).ToString(); //Define o nome do game object de acordo com seu índice + 1 para melhor visualização na Hierarchy.

            tilesPositions[i].transform.localScale = new Vector3((Random.Range(0, 2) == 0 ? -1 : 1), 1);

            lastCreatedTile = tilesPositions[i]; //Guarda o último objeto instanciado em lastCreatedTile.
        }

        if (_collectableCol)
        {
            var pst = Random.Range(0, emptyPositions.Count);

            for (var i = 0; i < emptyPositions.Count; i++) //Loop percorre a lista de posições que ficarão vazias na grade.
            {
                SetObjectToNull(tilesPositions[emptyPositions[i]]); //O bloco que tiver como índice a posição vazia da lista não possuirá sprite e colisor.

                if (pst == i)
                {
                    PositionCollectable(tilesPositions[emptyPositions[i]]);
                }
            }
        }

        else if (_swirlCol)
        {
            var pst = Random.Range(0, emptyPositions.Count);

            for (var i = 0; i < emptyPositions.Count; i++) //Loop percorre a lista de posições que ficarão vazias na grade.
            {
                SetObjectToNull(tilesPositions[emptyPositions[i]]); //O bloco que tiver como índice a posição vazia da lista não possuirá sprite e colisor.

                if (pst == i)
                {
                    PositionSwirl(tilesPositions[emptyPositions[i]]);
                }
            }
        }

        else
        {
            foreach (var blocker in emptyPositions)
            {
                SetObjectToNull(tilesPositions[blocker]);
            }
        }

        //foreach (var blocker in emptyPositions)
        //{
        //    SetObjectToNull(tilesPositions[blocker]);
        //}
    }

    //SetObjectToNull desativa o sprite e o colisor do objeto, além de deixá-lo sem tag.
    private static void SetObjectToNull(GameObject blocker)
    {
        blocker.GetComponent<SpriteRenderer>().sprite = null; //O bloco não possuirá sprite.
        blocker.GetComponent<Collider2D>().enabled = false; //O bloco não possuirá colisor.
        blocker.tag = "Untagged"; //Retira a tag do objeto.
    }

    private void PositionCollectable(GameObject blocker)
    {
        collectableController.InstantiateStar(blocker.transform.position);
    }

    private void PositionSwirl(GameObject blocker)
    {
        obstacleSwirlController.InstantiateSwirl(blocker.transform.position);
    }
}
