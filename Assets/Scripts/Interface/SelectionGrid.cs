﻿using System.Collections; //Utilizado para trabalhar com IEnumerator.
using System.Collections.Generic; //Utilizado para trabalhar com listas.
using System.Linq; //Utilizado para trabalhar com o método Select.
using System.Xml;
using System.Xml.Serialization;
using TMPro; //Utilizado para trabalhar com o plugin TextMeshPro.
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.SceneManagement; //Utilizado para tabalhar com o sistema de cenas da Unity.
using UnityEngine.UI; //Utilizado para trabalhar com o sistema de UI da Unity.

//Classe que define como será feita a disposição dos blocos nas telas de seleção.
public class SelectionGrid : MonoBehaviour
{
    public SFXController sfxController;
    public GridController gridController; //Referência de GridController.
	public GameObject buttonPrefab; //Referência do prefab do botão.
    public string portugueseTitleText; //Texto do título da página.
    public string englishTitleText; //Texto do título da página.
    public int executionOrder; //Variável que diz qual é posição desta página na ordem de execução.
    public Sprite englishButton; //Variável que diz qual é posição desta página na ordem de execução.
    public Sprite englishPressedButton; //Variável que diz qual é posição desta página na ordem de execução.

    public static int sbj;
    public static int grd;

    private int _buttonCount; //Quantidade de botões que serão gerados na tela.
	private List<string> _buttonsLabels; //Lista que abriga o rótulo/título de cada botão.

    //Função é chamada assim que o objeto atual é ativado.
	private void OnEnable()
	{
        List<Subject> subjects = QuestionManager.LoadXMLQuiz(); //Variável guarda todas as disciplinas presentes no arquivo XML.
		List<Grade> grades = subjects[GameController.subjectSelected].grades; //Variável guarda todas as séries presentes dentro da disciplina selecionada.
		List<Question> questions = grades[GameController.gradeSelected].questions; //Variável guarda todas as questões presentes dentro da série selecionada.

        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                gridController.titleText.text = portugueseTitleText; //O título da página recebe o título definido no Inspector;
                break;
            case 1:
                gridController.titleText.text = englishTitleText; //O título da página recebe o título definido no Inspector;
                break;
            default:
                break;
        }
        
		gridController.executionOrder = executionOrder; //A ordem de execução recebe o valor definido em GridController.

        if (!sfxController)
        {
            var sfx = GameObject.FindGameObjectWithTag("SFXController");

            if (sfx)
            {
                sfxController = sfx.GetComponent<SFXController>();
            }
        }

        switch (executionOrder) //Seleciona executionOrder e verifica:
		{
			case 1: //Caso 1:
				_buttonCount = subjects.Count; //A quantidade de botões será a quantidade de disciplinas.
				_buttonsLabels = subjects.Select(x => x.name).ToList(); //O rótulo/título de cada botão será pego da variável name dentro de cada disciplina.
				break; //Quebra do switch.
			case 2: //Caso 2:
				_buttonCount = grades.Count; //A quantidade de botões será a quantidade de séries.
				_buttonsLabels = grades.Select(x => x.index.ToString()).ToList(); //O rótulo/título de cada botão será pego da variável index dentro de cada série.
				break; //Quebra do switch.
			case 3: //Caso 3:
				_buttonCount = questions.Count; //A quantidade de botões será a quantidade de questões.
				_buttonsLabels = questions.Select(x => x.index.ToString()).ToList(); //O rótulo/título de cada botão será pego da variável index dentro de cada questão.
				break; //Quebra do switch.
			default: //Se não existir nada em executionOrder:
				break; //Quebra do switch.
		}

        //StartCoroutine(GetSize()); //Corrotina que chama a função GetSize, para que ela rode após o tempo determinado dentro da função.

	    var qst = MainMenu.scores.Where(x => x.SubjectID == sbj && x.GradeID == grd).ToList();

        for (int i = 0; i < _buttonCount; i++) //Loop percorre a quantidade de botões.
		{
			int index = i; //Deve ser criada uma nova variável que abriga o índice, senão ele só passa o último do loop na função abaixo.

			GameObject newButton = Instantiate(buttonPrefab, transform); //Instancia um botão utilizando a grade atual como pai.

			Button button = newButton.GetComponent<Button>(); //Armazena o componente Button do botão na variável.
            
            if (executionOrder == 1)
            {
                if (index >= 4) // Desabilita o botão de Ciências
                {
                    button.interactable = false;
                }

                if (index == 3) // Coloca bandeirinha dos EUA no botão "English"
                {
                    button.image.sprite = englishButton; 

                    button.spriteState = new SpriteState()
                    {
                        pressedSprite = englishPressedButton
                    };
                }
            }

		    if (executionOrder == 3)
		    {
		        if (!qst[i].IsLocked)
		        {
		            button.interactable = true;
		        }
		        else if (qst[i].IsLocked)
		        {
		            button.interactable = false;
		        }
            }

		    button.onClick.AddListener(() => { ActivateGrid(index); sfxController.OnButtonPressed(); }); //Aplica um listener na função onClick do botão que chama a função ActivateGrid com index como parâmetro.
            
			newButton.name = _buttonsLabels[i] + "Button"; //Altera o nome do botão na Hierarchy para melhor visualização.
			newButton.transform.GetComponentInChildren<TextMeshProUGUI>().text = _buttonsLabels[i]; //Altera o rótulo/título do botão para o atual do loop.
		}
	}

	//Função é chamada assim que o objeto atual é desativado.
	private void OnDisable()
	{
		foreach (Transform child in transform) //Loop percorre todos os filhos do objeto.
		{
			Destroy(child.gameObject); //Desativa o game object do filho atual do loop.
		}
	}

	//GetSize é rodado após os outros códigos para não acontecer problemas com os tamanhos dos botões.
	private IEnumerator GetSize()
	{
		yield return new WaitForSecondsRealtime(0.01f); // //Suspende a execução do código a seguir por pelo tempo definido ao lado.

		GridLayoutGroup grid = GetComponent<GridLayoutGroup>(); //Armazena o componente GridLayoutGroup do objeto atual na variável.
		Vector2 spacing = GetComponent<GridLayoutGroup>().spacing; //Armazena o espaçamento do GridLayoutGroup do objeto atual na variável.
		RectTransform rectTransform = transform.parent.GetComponent<RectTransform>(); //Armazena o componente RectTransform do pai do objeto atual na variável.
		float width = rectTransform.rect.width; //Armazena a largura do rectTransform.
		float height = rectTransform.rect.height; //Armazena a altura do rectTransform.
		int missingCells = grid.constraintCount - (_buttonCount % grid.constraintCount); //Faz o cálculo de quantos blocos restam para fechar a grade perfeita.
		int numCellsPerColumn = (_buttonCount + missingCells) / grid.constraintCount; //Número de células por coluna.

		Vector2 newSize = new Vector2((width - spacing.x * (grid.constraintCount - 1)) / grid.constraintCount, (height - spacing.y * (numCellsPerColumn - 1)) / numCellsPerColumn); //Calcula o tamanho do botão.
		grid.cellSize = newSize; //Define o tamanho de cada botão acessando o cellSize do GridLayoutGroup.
	}

	//ActivateGrid ativa a próxima grade/tela de seleção e desabilita a anterior.
	private void ActivateGrid(int index)
	{
		switch (executionOrder) //Seleciona executionOrder e verifica:
		{
			case 1: //Caso 1:
			    sbj = index;
				GameController.subjectSelected = index; //A disciplina atual recebe o índice do botão.
                GameController.gradeSelected = 0; //A série atual recebe o índice do botão.
                GameController.currentTopicName = _buttonsLabels[index];
                gridController.subjectsGrid.SetActive(false); //Desativa a tela de displinas.
				gridController.gradesGrid.SetActive(true); //Ativa a tela de séries.
				break; //Quebra do switch.
			case 2: //Caso 2:
			    grd = index;
				GameController.gradeSelected = index; //A série atual recebe o índice do botão.
				gridController.gradesGrid.SetActive(false); //Desativa a tela de séries.
				gridController.questionsGrid.SetActive(true); //Ativa a tela de questões.
				break; //Quebra do switch.
			case 3: //Caso 3:
				GameController.questionSelected = index; //A questão atual recebe o índice do botão.
                LoadGame();
                break; //Quebra do switch.
			default: //Se não existir nada em executionOrder:
				break; //Quebra do switch.
		}
	}

    public void LoadGame()
    {
        var soundLength = sfxController.OnButtonPressedThroughScenes();
        StartCoroutine(WaitForButtonClick(soundLength));
    }

    private IEnumerator WaitForButtonClick(float length)
    {
        while (length >= 0)
        {
            length -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        gridController.questionsGrid.SetActive(false);
        SceneManager.LoadScene(GameTags.SCN_Loading);
        LoadSceneController.nextScene = GameTags.SCN_MainGame;
    }
}