﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LC_MainMenu : MonoBehaviour
{
    [Header("Menu References")]
    public TextMeshProUGUI playButton;
    public TextMeshProUGUI instructionsButton;
    public TextMeshProUGUI shopButton;
    public TextMeshProUGUI optionsButton;
    public TextMeshProUGUI creditsButton;

    [Header("Instructions References")]
    public TextMeshProUGUI instructionsTitle;
    public TextMeshProUGUI instruction1;
    public TextMeshProUGUI instruction2;
    public TextMeshProUGUI instruction3;
    public TextMeshProUGUI instruction4;
    public TextMeshProUGUI instruction5;
    public TextMeshProUGUI instruction6;
    public TextMeshProUGUI instruction7;
    public TextMeshProUGUI instructionsBack;

    [Header("Shop References")]
    public TextMeshProUGUI shopTitle;
    public TextMeshProUGUI ship1;
    public TextMeshProUGUI ship2;
    public TextMeshProUGUI ship3;
    public TextMeshProUGUI ship4;
    public TextMeshProUGUI ship5;
    public TextMeshProUGUI shopBack;

    [Header("Options References")]
    public TextMeshProUGUI optionsHeader;
    public TextMeshProUGUI language;
    public TextMeshProUGUI graphics;
    public TextMeshProUGUI optionsBack;
    public TextMeshProUGUI resetProgress;
    public TextMeshProUGUI confirmResetBox;
    public TextMeshProUGUI progressDeleted;

    [Header("Credits References")]
    public TextMeshProUGUI creditsHeader;
    public TextMeshProUGUI programming;
    public TextMeshProUGUI artsAndGraphics;
    public TextMeshProUGUI professors;
    public TextMeshProUGUI creditsBack;

    public void Awake()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    public void OnLanguageChanged(int languageOption)
    {
        switch (languageOption)
        {
            case 0:
                Portuguese();
                break;
            case 1:
                English();
                break;
            default:
                break;
        }
    }

    private void Portuguese()
    {
        playButton.text = "Jogar";
        instructionsButton.text = "Instruções";
        shopButton.text = "Loja";
        optionsButton.text = "Opções";
        creditsButton.text = "Créditos";

        optionsHeader.text = "Opções";
        language.text = "Idioma";
        graphics.text = "Gráficos";
        optionsBack.text = "Voltar";
        resetProgress.text = "Apagar Progresso";
        confirmResetBox.text = "Você tem certeza de que deseja apagar seu progresso e começar de novo?";
        progressDeleted.text = "Progresso apagado com sucesso.";

        instructionsTitle.text = "Instruções";
        instruction1.text = "Clique em qualquer lugar da tela para mover o barco, marcando o ponto de destino com uma bóia.";
        instruction2.text = "Você também pode inclinar o dispositivo para os lados para movimentar o barco.";
        instruction3.text = "Para alternar entre inclinar e clicar, você pode usar o botão localizado no canto superior direito da tela.";
        instruction4.text = "Desvie dos obstáculos que encontrar pelo caminho,\ncomo bóias, lixo flutuante, barcos naufragados,\npedras e pequenas ilhas.";
        instruction5.text = "Se conseguir, desvie dos\nredemoinhos de água;\neles desaceleram seu barco.";
        instruction6.text = "Pontes e outros barcos\ntambém devem ser evitados!";
        instruction7.text = "Não esqueça de pegar a estrela no caminho para um bônus de tempo!";
        instructionsBack.text = "Voltar";

        shopTitle.text = "Loja";
        ship1.text = "Clássico";
        ship2.text = "Lancha";
        ship3.text = "Iate";
        ship4.text = "Veleiro";
        ship5.text = "Pirata";
        shopBack.text = "Voltar";

        creditsHeader.text = "Créditos";
        programming.text = "Programação";
        artsAndGraphics.text = "Artes";
        professors.text = "Professores Orientadores";
        creditsBack.text = "Voltar";

        ShopMenu.buyText = "COMPRAR";
        ShopMenu.selectText = "SELECIONAR";
    }

    private void English()
    {
        playButton.text = "Play";
        instructionsButton.text = "Instructions";
        shopButton.text = "Shop";
        optionsButton.text = "Options";
        creditsButton.text = "Credits";

        optionsHeader.text = "Options";
        language.text = "Language";
        graphics.text = "Graphics";
        optionsBack.text = "Back";
        resetProgress.text = "Delete Progress";
        confirmResetBox.text = "Are you sure you want to delete your game progress and start again?";
        progressDeleted.text = "Progress sucessfully deleted.";

        instructionsTitle.text = "Instructions";
        instruction1.text = "Click anywhere on the screen to move the ship, pointing out the destination with a buoy.";
        instruction2.text = "You may also tilt the device to move the ship.";
        instruction3.text = "To change between tilting or clicking, you may use the button located on the top right corner of the screen.";
        instruction4.text = "Avoid all obstacles you find, like floaters, trash,\nwrecked ships, rocks and small islands.";
        instruction5.text = "If you can, avoid\nwater swirls; they may\nslow down you ship.";
        instruction6.text = "Bridges and other ships\nshould also be avoided!";
        instruction7.text = "Don't forget to pick up the orange star for a bonus time of ten seconds!";
        instructionsBack.text = "Back";

        shopTitle.text = "Shop";
        ship1.text = "Classic";
        ship2.text = "Motorboat";
        ship3.text = "Yacht";
        ship4.text = "Sailboat";
        ship5.text = "Pirate Ship";
        shopBack.text = "Back";

        creditsHeader.text = "Credits";
        programming.text = "Programming";
        artsAndGraphics.text = "Graphics";
        professors.text = "Mentor Professors";
        creditsBack.text = "Back";

        ShopMenu.buyText = "BUY";
        ShopMenu.selectText = "SELECT";
    }
}
