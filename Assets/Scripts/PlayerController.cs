﻿using System;
using System.Collections; //Utilizado para trabalhar com IEnumerator.
using System.Collections.Generic; //Utilizado para trabalhar com listas.
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.EventSystems;
using UnityEngine.UI;

//Classe que define as configurações e realiza todas as ações do barco/jogador.
public class PlayerController : MonoBehaviour
{
	[Header("References")] //Cabeçalho do Inspector.
    //public BackgroundScroller backgroundScroller; //Referência de BackgroundController.
    public GameController gameController; //Referência de GameController.
    public AccelerometerController accelerometerController; //Referência de AccelerometerController.
    public CameraController cameraController; //Referência de CameraController.
    public CanvasController canvasController; //Referência de CanvasController.
	public QuestionManager questionManager; //Referência de QuestionManager.
	public Camera mainCamera; //Câmera principal.
	public GameObject navigationalBuoy; //Prefab da boia de navegação.
    public List<PolygonCollider2D> playerColliders;
    public DockController dockController;
    public Toggle accel;
    public GameObject wave;
    public RawImage background;
    public static bool movingForward;

    [Header("Player Properties")] //Cabeçalho do Inspector.
	[Range(1f, 10f)] //A variável abaixo pode no mínimo e no máximo os valores dentro de Range.
    public float speed; //Velocidade de movimentação do jogador.
    [Range(1f, 10f)] //A variável abaixo pode no mínimo e no máximo os valores dentro de Range.
    public float accelSpeed; //Velocidade de movimentação do jogador.

    [Range(0f, 500f)] //A variável abaixo pode no mínimo e no máximo os valores dentro de Range.
	public float obstaclePushForce; //Força que o objeto aplicará sobre o barco quando os dois colidirem.
	public float stopThreshold; //Limite de velocidade que o jogador deve atingir para testar se ele está parado.

	[Header("Hierarchy Properties")] //Cabeçalho do Inspector.
	public string buoysName; //Nome das boias na Hierarchy.
    public SpriteRenderer collisionPenalty;
    public SpriteRenderer collectableBonus;

    [HideInInspector] //Não mostra no Inspector.
	public bool playerIsMoving = false; //Variável testará se o jogador está ou não se movendo.

    public static float currentWaveY;
    private Rigidbody2D _body; 
    private GameObject _buoysParent; //Game object que abrigará todas as instâncias de tiles.
	private Vector2 _target; //Variável que guardará a posição de destino em X e Y.
	private readonly List<Vector2> _targetList = new List<Vector2>(); //Lista que armazena todas as posições de destino em X e Y.
	private readonly List<GameObject> _buoyList = new List<GameObject>(); //Lista que armazena todas as boias (game objects).
	private Vector2 _lastPos; //Variável que guardará a última posição onde uma boia foi criada.
	private GameObject _answerDock; //Game object que guardará a doca com a resposta escolhida pelo jogador.
	private bool _playerCollided = false; //Variável testará se o jogador acabou de colidir.
    private bool questionAnswered = false;
    private int accelUse;
    private Matrix4x4 _calibrationMatrix;
    private Vector3 wantedDeadZone;
    private Vector3 _InputDir;
    private bool clickingAllowed = true;
    private bool invincibilityPeriod = false;
    private Vector2 _desiredPosition;
    private bool _canTravelByAI = false;
    private bool lookingRight = true;

    //Start é chamado antes do Update.
    private void Start()
	{
        _body = GetComponent<Rigidbody2D>();
        CalibrateAccelerometer();

        if (!PlayerPrefs.HasKey(GameTags.PLP_Accelerometer))
        {
            accel.isOn = false;
            PlayerPrefs.SetInt(GameTags.PLP_Accelerometer, 0);
        }

        else if (PlayerPrefs.HasKey(GameTags.PLP_Accelerometer))
        {
            accelUse = PlayerPrefs.GetInt(GameTags.PLP_Accelerometer);

            switch (accelUse)
            {
                case 0:
                    accel.isOn = false;
                    break;
                case 1:
                    accel.isOn = true;
                    break;
                default:
                    break;
            }
        }
        
        GetComponent<SpriteRenderer>().sprite = GameController.shipSelected;

        gameObject.GetComponent<PolygonCollider2D>().points = playerColliders[GameController.shipSelectedIndex].points;

        _buoysParent = new GameObject {name = buoysName}; // Cria o objeto-pai que abrigará as boias, e define seu nome.

        questionAnswered = false;

        switch (PurchaseButton.waveIndex) // Posiciona a água na lateral do barco de acordo com o barco selecionado.
        {
            case 0:
                wave.transform.position = new Vector3(0, -0.30f, -2);
                currentWaveY = -0.30f;
                break;
            case 1:
                wave.transform.position = new Vector3(0, -0.37f, -2);
                currentWaveY = -0.37f;
                break;
            case 2:
                wave.transform.position = new Vector3(0, -0.47f, -2);
                currentWaveY = -0.47f;
                break;
            case 3:
                wave.transform.position = new Vector3(0, -0.52f, -2);
                currentWaveY = -0.52f;
                break;
            case 4:
                wave.transform.position = new Vector3(0, -0.44f, -2);
                currentWaveY = -0.44f;
                break;
            default:
                wave.transform.position = new Vector3(0, -0.30f, -2);
                currentWaveY = -0.30f;
                break;
        }
    }

    private void CalibrateAccelerometer()
    {
        wantedDeadZone = (Vector3)Input.acceleration;
        Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0f, 0f, 0f), wantedDeadZone);
        Matrix4x4 matrix = Matrix4x4.TRS(Vector3.zero, rotateQuaternion, new Vector3(1f, 1f, 1f));
        _calibrationMatrix = matrix.inverse;
    }
    
    //Update é chamado a cada frame.
    private void Update()
	{
        if (transform.position.y >= 5.25f || transform.position.y <= -5.25f)
        {
            StartCoroutine(RepositionShipToZero());
        }        

        if (accel.isOn)
        {
            wave.SetActive(true);

            transform.eulerAngles = new Vector2(0, 0); // Vira o barco pra frente.
            collisionPenalty.flipX = false;
            collectableBonus.flipX = false;
        }        

        if (!gameController.isNavigationBlocked && accel.isOn)
        {
            Vector3 move = Vector3.zero;
            
            if (transform.position.x >= -5 && Input.acceleration.x <= 0)
            {
                move.x = Input.acceleration.x * (Time.deltaTime * accelSpeed);
            }
            if (transform.position.x <= (dockController._quaysPos.x * 1.025f) && Input.acceleration.x >= 0)
            {
                move.x = Input.acceleration.x * (Time.deltaTime * accelSpeed);
            }

            if (transform.position.y >= -4.5f && Input.acceleration.y <= 0)
            {
                move.y = Input.acceleration.y * (Time.deltaTime * accelSpeed);
            }

            if (transform.position.y <= 4.5f && Input.acceleration.y >= 0)
            {
                move.y = Input.acceleration.y * (Time.deltaTime * accelSpeed);
            }

            transform.Translate(move);
        }

        //Se a navegação não está bloqueada, E for executado um clique com o botão esquerdo do mouse/um toque na tela:
        else if (!gameController.isNavigationBlocked && !accel.isOn && Input.GetMouseButtonDown(0)) 
		{
            Vector2 origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = Vector2.zero;

            RaycastHit2D hit = Physics2D.Raycast(origin, direction);

            if (EventSystem.current.IsPointerOverGameObject()) //Se o clique atingiu algum game object:
            {
				return; //Retorna e não realiza o resto.
            }
            
            if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) //Se o toque (tablet) atingiu algum game object:
            {
                return; //Retorna e não realiza o resto.
            }
            
            if (hit) // Se houve clique:
            {
                if (hit.collider.name == "Constraints2D") 
                {
                    return; // Retorna se o clique for na borda da tela.
                }
            }

            if (clickingAllowed)
            {
                _target = mainCamera.ScreenToWorldPoint(Input.mousePosition); //Atribui a posição do clique do mouse à posição de destino (target).
            }

            //backgroundScroller.speed = GetBackgroundSpeed(_target); //Recebe a velocidade retornada em getBackgroundSpeed passando target como parâmetro.

            if (_target != _lastPos) //Se a posição de destino for diferente da última posição criada:
			{
			    RemoveAllBuoys(); //Remove todas as boias no cenário.

			    _targetList.Add(_target); //Adiciona o Vector2 à lista.

			    GameObject newInstance = Instantiate(navigationalBuoy, _target, Quaternion.identity, _buoysParent.transform); //Instancia uma boia na posição do clique do mouse.

			    _buoyList.Add(newInstance); //Adiciona o game object à lista.

			    newInstance.name = "Buoy"; //Define o nome do game object de acordo com seu índice para melhor visualização na Hierarchy.

			    _lastPos = _target; //Guarda a última posição criada.
			}
		}

        if (_targetList.Count <= 0 && !accel.isOn)
        {
            wave.SetActive(false);

            if (!lookingRight) 
            {
                transform.eulerAngles = new Vector3(0, 180, 0); // Vira o barco pra trás.
                collisionPenalty.flipX = true;
                collectableBonus.flipX = true;
            }

            else if (lookingRight)
            {
                transform.eulerAngles = new Vector3(0, 0, 0); // Vira o barco pra frente.
                collisionPenalty.flipX = false;
                collectableBonus.flipX = false;
            }

            _canTravelByAI = false;
        }

        else if (_targetList.Count >= 1)
        {
            wave.SetActive(true);
        }

        if (_targetList.Count >= 1 && !_playerCollided) //Se a quantidade de boias for maior que 1:
		{
			playerIsMoving = true; //Define que o jogador está se movendo.

            if (_target.x < transform.position.x) // Quando o X do target (boia) for menor que o do barco:
            {
                transform.eulerAngles = new Vector3(0, 180,0); // Vira o barco pra trás.
                collisionPenalty.flipX = true;
                collectableBonus.flipX = true;

                lookingRight = false;
            }

            if (_target.x > transform.position.x) // Quando o X do target (boia) for maior que o do barco:
            {
                transform.eulerAngles = new Vector3(0, 0, 0); // Vira o barco pra frente.
                collisionPenalty.flipX = false;
                collectableBonus.flipX = false;

                lookingRight = true;
            }

            _canTravelByAI = true;

            if (transform.position.x == _lastPos.x && transform.position.y == _lastPos.y) //Se o barco e a boia estiverem na mesma posição:
			{
				DestroyBuoy(); //Chama a função para destruir a boia atual.
			}
		}
		else //Senão:
		{
            playerIsMoving = false; //Define que o jogador não está se movendo.

            if (GetComponent<Rigidbody2D>().velocity.magnitude <= stopThreshold && _playerCollided) //Se a velocidade do corpo físico for menor ou igual a stopThreshold e o jogador acabou de colidir:
			{
				_playerCollided = false; //Define que o jogador não está colidindo com nada.
			}
		}

		if (_answerDock != null) //Se a posição da doca escolhida não for nula:
		{
			transform.position = Vector2.MoveTowards(transform.position, _answerDock.transform.position, speed * Time.deltaTime); //Move o objeto da posição atual para o centro do círculo de acordo com a velocidade definida em speed.
		}
	}

    private void FixedUpdate()
    {
        if (_canTravelByAI)
        {
            _desiredPosition = Vector2.MoveTowards(transform.position, _lastPos, speed * Time.fixedDeltaTime);
            _body.MovePosition(_desiredPosition);//Move o objeto da posição atual para a posição de destino (target) de acordo com a velocidade definida em speed.
        }
    }

    //Recebe um Vector2 como parâmetro para retornar a velocidade que será aplicada à movimentação da textura.
    private float GetBackgroundSpeed(Vector2 target)
	{
		return (target.x - transform.position.x) / cameraController.cameraWidth; //Retorna a velocidade de movimentação da textura, que será a relação da distância entre o destino e o barco pelo tamanho da câmera.
	}

	//Testa quando o objeto entra em colisão com um trigger.
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Dock" && !questionAnswered) //Se a colisão for com algum GameObject com a tag "Dock":
        {
            clickingAllowed = false;

			_answerDock = collision.gameObject; //A doca de resposta recebe a doca colidida pelo jogador.

            questionAnswered = true;

			StartCoroutine(Result(collision.gameObject.transform.parent.gameObject)); //Corrotina que chama a função Result passando o pai do objeto colidido como parâmetro. Nesse caso, o pai é o cais (quay) e o objeto é a doca (dock).
		}

	    if (collision.gameObject.tag != "Swirl") return;

	    speed /= 2;
	    accelSpeed /= 2;
	}

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Swirl") return;

        speed *= 2;
        accelSpeed *= 2;
    }

    //Result aguarda o tempo determinado na variável gameController.resultDelay e chama questionManager.PlayerAnswer após esse tempo.
    private IEnumerator Result(GameObject dockParent)
	{
		yield return new WaitForSecondsRealtime(gameController.resultDelay); //Suspende a execução do código a seguir pelo tempo determinado em gameController.resultDelay (pode ser editado no Inspector).

		questionManager.PlayerAnswer(dockParent.GetComponent<DockAnswer>().answer); //Chama questionManager.PlayerAnswer com a string answer atribuída àquela doca.
	}

	//Testa quando o objeto entra em colisão com algo.
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if ((collision.gameObject.tag == "Obstacle" || collision.gameObject.tag == "Quay") && !invincibilityPeriod) //Se a colisão for com algum GameObject com a tag "Obstacle" ou "Quay":
		{
            if (accel.isOn)
            {
                StartCoroutine(PauseAccelerometer());
            }

            if (!accel.isOn)
            {
                StartCoroutine(StopClicking());
            }

            _playerCollided = true; //Define que o jogador acabou de colidir com algo.

            gameController.counter = gameController.counter - 5;

            canvasController.UpdateTimerAfterCollision();

            collisionPenalty.gameObject.SetActive(false);

            collisionPenalty.gameObject.SetActive(true);

            GameObject.FindGameObjectWithTag("SFXController").SendMessage("OnCollision");

            // Impedir que o player crie uma nova bóia enquanto está sendo jogado para trás depois da colisão.

            Vector2 direction = transform.position - collision.transform.position; //Variável armazena a distância entre o jogador e o objeto colisor.
			direction.Normalize(); //Mantém a magnitude do vetor em 1.

			GetComponent<Rigidbody2D>().AddForce(direction * obstaclePushForce * 5); //Adiciona uma força de acordo com a direção do vetor e obstaclePushForce.

			RemoveAllBuoys(); //Chama a função que remove todas as bóias.
		}

	    if (collision.gameObject.tag == "Obstacle" && !invincibilityPeriod)
	    {
	        StartCoroutine(Damage());
        }

        if (collision.gameObject.tag == "Collectable")
	    {
	        Destroy(collision.gameObject);

	        gameController.counter = gameController.counter + 10;

	        collectableBonus.gameObject.SetActive(false);

	        collectableBonus.gameObject.SetActive(true);

	        canvasController.UpdateTimerAfterCollision();

	        GameObject.FindGameObjectWithTag("SFXController").SendMessage("OnCollectable");
        }
	}

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Constraints")
        {
            RemoveAllBuoys(); //Remove todas as boias no cenário.
        }
    }

    private IEnumerator Damage()
    {
        Color playerColor = GetComponent<SpriteRenderer>().color;

        playerColor.a = 0.75f;
        GetComponent<SpriteRenderer>().color = playerColor;

        invincibilityPeriod = true;

        yield return new WaitForSeconds(0.75f);

        invincibilityPeriod = false;

        playerColor.a = 1f;
        GetComponent<SpriteRenderer>().color = playerColor;
    }

    private IEnumerator PauseAccelerometer()
    {
        accel.isOn = false;

        yield return new WaitForSeconds(0.25f);

        accel.isOn = true;
    }

    private IEnumerator StopClicking()
    {
        clickingAllowed = false;

        yield return new WaitForSeconds(0.25f);

        clickingAllowed = true;
    }

    //Função remove todas as bóias instanciadas.
    private void RemoveAllBuoys()
	{
		for (int i = _buoyList.Count - 1; i >= 0; i--) //Loop percorre todos os itens da lista de trás para frente para destrui-los posteriormente.
		{
			Destroy(_buoyList[i].gameObject); //Destrói o objeto instanciado.
			_buoyList.Remove(_buoyList[i]); //Remove o índice i para liberar a próxima boia.
			_targetList.Remove(_targetList[i]); //Remove o índice i para liberar a próxima posição.
		}
	}

	//Destrói a boia do índice 0 na lista de boias.
	private void DestroyBuoy()
	{
		Destroy(_buoyList[0].gameObject); //Destrói o objeto instanciado.
		_buoyList.Remove(_buoyList[0]); //Remove o índice 0 para liberar a próxima boia.
		_targetList.Remove(_targetList[0]); //Remove o índice 0 para liberar a próxima posição.
	}

    /*
        if (_targetList.Count > 0) //Se existirem destinos para o jogador se movimentar:
		{
			//backgroundScroller.speed = GetBackgroundSpeed(_targetList[0]); //Recebe a velocidade retornada em getBackgroundSpeed passando _targets[0] como parâmetro.
		}
		else //Se não existir nenhum destino:
		{
			backgroundScroller.speed = 0; //A velocidade de movimentação da textura passa a ser zero.
		}
    */

    private IEnumerator RepositionShipToZero()
    {
        yield return new WaitForSeconds(1);

        transform.position = new Vector3(0, 0, -2);
        mainCamera.transform.position = new Vector3(0, 0, -10);
    }
    
    /*
Vector3 GetAccelerometer(Vector3 accelerator)
{
    Vector3 accel = this.calibrationMatrix.MultiplyVector(accelerator);
    return accel;
}

private void Update()
{
    _InputDir = getAccelerometer(Input.acceleration);

    if (!gameController.isNavigationBlocked && accel.isOn)
    {
        Vector3 move = Vector3.zero;

        Debug.Log(Input.acceleration);


        if (transform.position.x >= -5 && Input.acceleration.x <= 0)
        {
            move.x = Input.acceleration.x * (Time.deltaTime * accelSpeed);
        }
        if (transform.position.x <= (dockController._quaysPos.x * 1.025f) && Input.acceleration.x >= 0)
        {
            move.x = Input.acceleration.x * (Time.deltaTime * accelSpeed);
        }

        if (transform.position.y >= -4.5f && Input.acceleration.y <= 0)
        {
            move.y = Input.acceleration.y * (Time.deltaTime * accelSpeed);
        }
        if (transform.position.y <= 4.5f && Input.acceleration.y >= 0)
        {
            move.y = Input.acceleration.y * (Time.deltaTime * accelSpeed);
        }

        transform.Translate(move);
    }
*/
}


