﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleShipController : MonoBehaviour
{
    public GameObject obstacleShip;
    private GameObject instance;
    private Vector3 spawn;
    
    public void InstantiateShip(Vector2 emptyColumnPos)
    {
        spawn = new Vector3(emptyColumnPos.x, 0, 0);
        instance = Instantiate(obstacleShip, spawn, new Quaternion(0, 0, 0, 0));
    }

    //private void Update()
    //{
    //    if (instance != null)
    //    {
    //        if (instance.transform.position != destination)
    //        {
    //            instance.transform.position = Vector3.MoveTowards(instance.transform.position, destination, (speed * Time.deltaTime));
    //        }
    //        if (instance.transform.position == destination)
    //        {
    //            instance.transform.position = spawn;
    //        }
    //    }
    //}
}
