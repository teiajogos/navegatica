﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro; 
using UnityEngine;
using UnityEngine.SceneManagement;

//Classe que serve como controlar do jogo. Define as configurações principais e possui as funções que determinam o estado atual do gameplay. 
public class GameController : MonoBehaviour
{
	[Header("References")] //Cabeçalho do Inspector.
    public CanvasController canvasController;
    public TileController tileController;
    public SFXController sfxController;
    public GameObject victory; //Referência do Game object com a tela de vitória.
	public GameObject defeat; //Referência do Game object com a tela de derrota.
    public GameObject lastQuestion; //Referência do Game object com a tela de vitória na última questão.
    public GameObject gameOver; //Referência do Game object com a tela de game over.
    public GameObject pause; //Referência do Game object com a tela de pause.
    public GameObject question; //Referência do Game object com a tela de questões.
    public GameObject readQuestion; //Referência do Game object com a tela de questões.
    public TextMeshProUGUI victoryScoreText; //Texto da pontuação final do jogador.
    public TextMeshProUGUI lastQuestionScoreText; //Texto da pontuação final do jogador.
    public List<TextMeshProUGUI> playerScoreText; //Texto da pontuação final do jogador.
    public List<TextMeshProUGUI> highscoreOnQuestion; //Texto da pontuação final do jogador.
    public List<TextMeshProUGUI> currentLevelInfo;
    public AudioSource gameAudioSource; //Pontuação do jogador no nível atual.
    public static string currentTopicName;

    [Header("Game Settings")] //Cabeçalho do Inspector.
	public float resultDelay; //Tempo de espera até aparecer as telas do resultado (correto ou incorreto).
	[HideInInspector] public int counter; //Temporizador.

    public static int userSelected;
	public static int subjectSelected; //Disciplina selecionada.
	public static int gradeSelected; //Série selecionada.
	public static int questionSelected; //Questão selecionada.
    public static int questionHighScore;

	public static Sprite shipSelected; //Navio selecionado.
    public static int shipSelectedIndex;
    public static int playerScore = 0; //Pontuação do jogador.
    public static int levelScore; //Pontuação do jogador no nível atual.

    [Range(1, 10)] //A variável abaixo pode no mínimo e no máximo os valores dentro de Range.
	public int scoreMultiplier; //Multiplicador de pontos.

    public Sprite classicShip; //Sprite do barco clássico.
    public bool isNavigationBlocked;

    //Chamado ao iniciar da cena (antes do Start).
    private void Awake()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                foreach (var item in currentLevelInfo)
                {
                    item.text = currentTopicName + ": Nível " + (gradeSelected + 1) + " - Questão " + (questionSelected + 1);
                }
                break;
            case 1:
                foreach (var item in currentLevelInfo)
                {
                    item.text = currentTopicName + ": Level " + (gradeSelected + 1) + " - Question " + (questionSelected + 1);
                }
                break;
            default:
                break;
        }

        counter = 30 + (5 * (questionSelected + 1));

        Screen.orientation = LoadSceneController.ornt;

        if (shipSelected == null) // Se não há barco selecionado pelo jogador:
        {
            shipSelected = classicShip; //O barco selecionado automaticamente será o barco clássico.
            shipSelectedIndex = 0;
        }

        var menuAudio = GameObject.FindGameObjectWithTag("AudioController");

        if (menuAudio != null)
        {
            menuAudio.SendMessage("OnGameStart");
        }

        gameAudioSource.volume = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);
        gameAudioSource.Play();
    }

    //Start é chamado no momento em que o GameController é ativado na cena.
    private void Start()
    {
        pause.SetActive(false); //Define que a tela de pause está desativada.
        ActivateScreen(question);
    }
        
    //Ativa a tela (game object) passado como parâmetro.
    public void ActivateScreen(GameObject panel)
    {
        List<Subject> subjects = QuestionManager.LoadXMLQuiz();

        Time.timeScale = 0; //Define que o tempo está completamente parado.

		canvasController.isCounting = false; //Define que o contador do CanvasController está parado.

        this.isNavigationBlocked = true;

		panel.SetActive(true); //Define que a tela panel está ativa.

        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        var actualQ = MainMenu.scores.FirstOrDefault(x => x.QuestionID == questionSelected && x.GradeID == gradeSelected && x.SubjectID == subjectSelected);
        var nextQ = MainMenu.scores.FirstOrDefault(x => x.QuestionID == questionSelected + 1 && x.GradeID == gradeSelected && x.SubjectID == subjectSelected);

        if (panel == victory) //Se panel for a tela de vítória normal, ou vitória na última questão:
        {
            gameAudioSource.Stop();
            levelScore = (counter * 2) * scoreMultiplier;
            playerScore = playerScore + ((counter * 2) * scoreMultiplier); //Faz com que a pontuação do jogador seja o que restou do contador vezes o multiplicador.

            switch (lng)
            {
                case 0:
                    victoryScoreText.text = "PONTOS RECEBIDOS: " + levelScore.ToString(); //O texto da pontuação final do jogador recebe _playerScore.
                    break;
                case 1:
                    victoryScoreText.text = "POINTS EARNED: " + levelScore.ToString(); //O texto da pontuação final do jogador recebe _playerScore.
                    break;
                default:
                    break;
            }

            nextQ.IsLocked = false;

            if (actualQ.HighScore < levelScore)
            {
                actualQ.HighScore = levelScore;
            }

            gameOver.SetActive(false);

            GameObject.FindGameObjectWithTag("SFXController").SendMessage("OnCorrectAnswer");
        }

        if (panel == lastQuestion) //Se panel for a tela de vítória normal, ou vitória na última questão:
        {
            gameAudioSource.Stop();
            levelScore = (counter * 2) * scoreMultiplier;
            playerScore = playerScore + ((counter * 2) * scoreMultiplier); //Faz com que a pontuação do jogador seja o que restou do contador vezes o multiplicador.

            switch (lng)
            {
                case 0:
                    lastQuestionScoreText.text = "PONTOS RECEBIDOS: " + levelScore.ToString(); //O texto da pontuação final do jogador recebe _playerScore.
                    break;
                case 1:
                    lastQuestionScoreText.text = "POINTS EARNED: " + levelScore.ToString(); //O texto da pontuação final do jogador recebe _playerScore.
                    break;
                default:
                    break;
            }

            nextQ.IsLocked = false;

            if (actualQ.HighScore < levelScore)
            {
                actualQ.HighScore = levelScore;
            }

            gameOver.SetActive(false);

            GameObject.FindGameObjectWithTag("SFXController").SendMessage("OnCorrectAnswer");
        }

        if (panel == defeat || panel == gameOver)
        {
            if (question.activeSelf)
            {
                question.SetActive(false);
            }

            victory.SetActive(false);
            lastQuestion.SetActive(false);

            gameAudioSource.Stop();
            GameObject.FindGameObjectWithTag("SFXController").SendMessage("OnWrongAnswer");
        }

        if (panel == readQuestion) //Se panel for a tela de pergunta:
        {
            Time.timeScale = 1; //Define que o tempo não está parado nessa tela.

            canvasController.isCounting = true; //Define que o contador do CanvasController está contando.
        }

        foreach (var item in playerScoreText)
        {
            switch (lng)
            {
                case 0:
                    item.text = "SEUS PONTOS: " + playerScore.ToString();
                    break;
                case 1:
                    item.text = "YOUR POINTS: " + playerScore.ToString();
                    break;
                default:
                    break;
            }
        }

        foreach (var item in highscoreOnQuestion)
        {
            switch (lng)
            {
                case 0:
                    item.text = "SUA MAIOR PONTUAÇÃO: " + actualQ.HighScore;
                    break;
                case 1:
                    item.text = "YOUR HIGHEST SCORE: " + actualQ.HighScore;
                    break;
                default:
                    break;
            }
        }
    }
    
    public void UnblockNavigation()
    {
        this.isNavigationBlocked = false;
    }

    //Função que retorna ao jogo após o pause.
    public void ResumeGame()
	{
		Time.timeScale = 1; //Define que o tempo está de volta ao normal.

		canvasController.isCounting = true; //Define que o contador do CanvasController está contando.

        pause.SetActive(false); //Define que a tela de pause está desativada.
    }

    public void StartGame()
    {
        Time.timeScale = 1; //Define que o tempo está de volta ao normal.

        canvasController.isCounting = true; //Define que o contador do CanvasController está contando.

        question.SetActive(false); //Define que a tela de quetsão inicial está desativada.
    }

    public void NextStage()
    {
        List<Subject> subjects = QuestionManager.LoadXMLQuiz();

        if (subjects[subjectSelected].grades[gradeSelected].questions[questionSelected] != null)
		{
			questionSelected++;
            RestartGame();
        }
		else
		{
			GoToSceneThroughLoad(GameTags.SCN_StageSelection);
		}

		Time.timeScale = 1; //Define que o tempo está de volta ao normal.
	}

    public void GoToSceneThroughLoad(string scene)
    {
        var soundLength = sfxController.OnButtonPressedThroughScenes();
        StartCoroutine(WaitForButtonClickThroughLoad(soundLength, scene));

        Time.timeScale = 1;
    }

    private IEnumerator WaitForButtonClickThroughLoad(float length, string sceneName)
    {
        while (length >= 0)
        {
            length -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(GameTags.SCN_Loading);
        LoadSceneController.nextScene = sceneName;
    }

    public void RestartGame()
    {
        var soundLength = sfxController.OnButtonPressedThroughScenes();
        StartCoroutine(WaitForButtonClick(soundLength));

        Time.timeScale = 1;
    }

    private IEnumerator WaitForButtonClick(float length)
    {
        while (length >= 0)
        {
            length -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}