﻿using System.Linq; //Utilizado para trabalhar com o método Select.
using UnityEditor; //Utilizado para trabalhar com Custom Editors.
using UnityEditorInternal; //Utilizado para trabalhar com ReorderableList.
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.

//Classe que cria um Custom Editor para facilitar a edição das disciplinas, séries e questões.
[CustomEditor(typeof(QuestionManager))] //Define que a classe está editando o script QuestionManager.
public class QuestionEditor : Editor
{
	private QuestionManager _target; //Definido que o objeto inspecionado será QuestionManager.
	private ReorderableList _subjectList; //Lista que abrigará todas as disciplinas.
	private ReorderableList _gradeList; //Lista que abrigará todas as séries.
	private ReorderableList _questionList; //Lista que abrigará todas as questões.
	private int _subjectSelected; //Disciplina selecionada no Inspector.
	private int _gradeSelected; //Sériee selecionada no Inspector.
	private int _questionSelected; //Questão selecionada no Inspector.
	private bool _subjectDropdownButton; //Booleana que abriga o botão suspenso das disciplinas.
	private bool _gradeDropdownButton; //Booleana que abriga o botão suspenso das séries.
	private bool _questionDropdownButton; //Booleana que abriga o estado do botão suspenso das questões.
	private bool _subjectEditButtonActive; //Testa se o botão de edição das disciplinas está ativo.
	private bool _gradeEditButtonActive; //Testa se o botão de edição das séries está ativo.
	private bool _questionEditButtonActive; //Testa se o botão de edição das questões está ativo.

	//Função é chamada assim que o editor é ativado.
	private void OnEnable()
	{
        _target = (QuestionManager)target; //Define que _target será o alvo inspecionado QuestionManager. FFODA SE LUCAS, NEM TANTO NÉ
        _target.subjects = QuestionManager.LoadData(Application.dataPath + "/Resources/Data/" + "XML_Quiz.xml");
    }

	//Define as propriedades dos objetos Box (cor da fonte e opacidade do fundo).
	private void BoxProperties(Color fontColor, float backgroundAlpha)
	{
		GUI.skin.box.normal.background = new Texture2D(64, 64); //O fundo da Box recebe uma textura 64 x 64.

		for (int y = 0; y < GUI.skin.box.normal.background.height; ++y) //Loop percorre todos os pixels do eixo y da textura.
		{
			for (int x = 0; x < GUI.skin.box.normal.background.width; ++x) //Loop percorre todos os pixels do eixo x da textura.
			{
				Color color = Color.white; //Define que a cor da Box é branca.
				color.a = backgroundAlpha; //O alpha da cor é backgroundAlpha.
				GUI.skin.box.normal.background.SetPixel(x, y, color); //Pinta o pixel atual do loop de acordo com a cor e opacidade definida em color.
			}
		}

		GUI.skin.box.normal.background.Apply(); //Aplica as modificações no fundo.

		GUI.skin.box.normal.textColor = fontColor; //Define a cor do texto da Box.
		GUI.skin.box.fontStyle = FontStyle.Bold; //Define que o texto é em negrito.
	}

	//Define como será o cabeçalho do segmento (disciplinas, séries, questões) com o título e qual será a booleana que abriga o botão de edição.
	private bool SegmentHeader(string title, bool dropdownButton)
	{
        GUILayout.BeginHorizontal(); //Inicia o layout horizontal.

		GUILayout.Box(title, GUILayout.ExpandWidth(true)); //Cria uma Box com o título definido em title e expande a largura até o limite.

		GUI.color = Color.yellow; //Define a cor da GUI para amarelo.

		if (GUILayout.Button("EDIT", GUILayout.MaxWidth(50))) //Cria um botão de edição do segmento com 50 de largura e testa se ele foi clicado:
		{
			dropdownButton = !dropdownButton; //Ao ser clicado ele troca o estado a atual de dropdownButton.
		}

		GUILayout.EndHorizontal(); //Termina o layout horizontal.

		GUI.color = Color.white; //Define a cor da GUI para branco.

		return dropdownButton; //Retorna o valor atual de dropdownButton.
	}

	private void UpdateList(ReorderableList list)
	{
		serializedObject.Update();
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
	}
    


    public override void OnInspectorGUI()
	{
        
        #region GUI Properties

        base.OnInspectorGUI();

		float spaceBetweenSegments = 10;

		GUI.skin.box.alignment = TextAnchor.MiddleLeft;
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;

		BoxProperties(Color.black, 0.5f);

		GUILayout.Space(spaceBetweenSegments);

		GUILayout.Label("QUESTION EDITOR");

		GUILayout.Space(spaceBetweenSegments);

		#endregion

		#region Subjects Grid

		#region Title

		_subjectDropdownButton = SegmentHeader("SUBJECTS", _subjectDropdownButton);

		#endregion
		#region Grid
		if (!_subjectDropdownButton)
		{
			_subjectEditButtonActive = false;

			if (_target.subjects.Count > 0)
			{
				_subjectSelected = GUILayout.SelectionGrid(_subjectSelected, _target.subjects.Select(x => x.name).ToArray(), 3);
			}

			GUILayout.Space(spaceBetweenSegments);
		}
		#endregion
		#region Reorderable List
		if (_subjectDropdownButton)
		{
			_gradeDropdownButton = false;
			_questionDropdownButton = false;

			if (!_subjectEditButtonActive)
			{
				_subjectList = new ReorderableList(serializedObject, serializedObject.FindProperty("subjects"), true, true, true, true);

				_subjectList.drawHeaderCallback = rect =>
				{
					EditorGUI.LabelField(rect, "Drag to reorder | Click to rename | + to add | - to remove selection", EditorStyles.boldLabel);
				};

				_subjectList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
				{
					_target.subjects[index].name = EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), _target.subjects[index].name);
				};

				_subjectEditButtonActive = true;
			}

			UpdateList(_subjectList);
		}
		#endregion

		#endregion

		#region Grades Grid

		#region Title

		_gradeDropdownButton = SegmentHeader("GRADES", _gradeDropdownButton);

		#endregion
		#region Grid
		if (!_gradeDropdownButton)
		{
			_gradeEditButtonActive = false;

			if (_target.subjects[_subjectSelected].grades.Count > 0)
			{
				_gradeSelected = GUILayout.SelectionGrid(_gradeSelected, _target.subjects[_subjectSelected].grades.Select(x => x.index.ToString()).ToArray(), 3);
			}

			GUILayout.Space(spaceBetweenSegments);
		}
		#endregion
		#region Reorderable List
		if (_gradeDropdownButton)
		{
			_subjectDropdownButton = false;
			_questionDropdownButton = false;

			if (!_gradeEditButtonActive)
			{
				string gradeIndex = "subjects.Array.data[" + _subjectSelected + "].grades";

				_gradeList = new ReorderableList(serializedObject, serializedObject.FindProperty(gradeIndex), true, true, true, true);

				_gradeList.drawHeaderCallback = rect =>
				{
					EditorGUI.LabelField(rect, "Drag to reorder | + to add | - to remove selection", EditorStyles.boldLabel);
				};

				_gradeList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
				{
					EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), (index + 1).ToString());
					_target.subjects[_subjectSelected].grades[index].index = index + 1;
				};

				_gradeEditButtonActive = true;
			}

			UpdateList(_gradeList);
		}
		#endregion

		#endregion

		#region Questions Grid

		#region Title

		_questionDropdownButton = SegmentHeader("QUESTIONS", _questionDropdownButton);

		#endregion
		#region Grid
		if (!_questionDropdownButton)
		{
			_questionEditButtonActive = false;

			if (_target.subjects[_subjectSelected].grades[_gradeSelected].questions.Count > 0)
			{
				int numRows;

				if (_target.subjects[_subjectSelected].grades[_gradeSelected].questions.Count % 10 != 0)
					numRows = 1 + _target.subjects[_subjectSelected].grades[_gradeSelected].questions.Count / 10;
				else
					numRows = _target.subjects[_subjectSelected].grades[_gradeSelected].questions.Count / 10;

				int question = _questionSelected;

				_questionSelected = GUILayout.SelectionGrid(_questionSelected, _target.subjects[_subjectSelected].grades[_gradeSelected].questions.Select(x => x.index.ToString()).ToArray(), 10, GUILayout.Height(40 * numRows));

				if (question != _questionSelected)
				{
					GUI.FocusControl(null);
				}
			}

			GUILayout.Space(spaceBetweenSegments);
		}
		#endregion
		#region ReorderableList
		if (_questionDropdownButton)
		{
			_subjectDropdownButton = false;
			_gradeDropdownButton = false;

			if (!_questionEditButtonActive)
			{
				string questionIndex = "subjects.Array.data[" + _subjectSelected + "].grades.Array.data[" + _gradeSelected + "].questions";

				_questionList = new ReorderableList(serializedObject, serializedObject.FindProperty(questionIndex), true, true, true, true);

				_questionList.drawHeaderCallback = rect =>
				{
					EditorGUI.LabelField(rect, "Drag to reorder | Click to rename | + to add | - to remove selection", EditorStyles.boldLabel);
				};

				_questionList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
				{
					float rectSize = 30;
					EditorGUI.LabelField(new Rect(rect.x, rect.y, rectSize, EditorGUIUtility.singleLineHeight), (index + 1).ToString(), EditorStyles.miniButtonMid);
					_target.subjects[_subjectSelected].grades[_gradeSelected].questions[index].question = EditorGUI.TextField(new Rect(rect.x + rectSize, rect.y, rect.width - rectSize, EditorGUIUtility.singleLineHeight), _target.subjects[_subjectSelected].grades[_gradeSelected].questions[index].question);
					_target.subjects[_subjectSelected].grades[_gradeSelected].questions[index].index = index + 1;
				};

				_questionEditButtonActive = true;
			}

			UpdateList(_questionList);
		}
		#endregion

		#endregion

		#region Question Editor

		BoxProperties(Color.white, 0.1f);

		GUILayout.BeginVertical("Box");

		GUILayout.BeginHorizontal();

		GUILayout.Label("QUESTION " + (_questionSelected + 1), GUILayout.ExpandWidth(true));

		GUILayout.EndHorizontal();

		_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].question = EditorGUILayout.TextArea(_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].question, GUILayout.Height(80));

		GUILayout.Space(spaceBetweenSegments);

		for (int i = 0; i < _target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].answers.Count; i++)
		{
			GUILayout.BeginHorizontal();
			{
				GUILayout.BeginHorizontal(GUILayout.Width(40));

				GUI.color = Color.red;

				if (GUILayout.Button("✘", GUILayout.MaxWidth(20)))
				{
					_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].answers[i] = "";
				}

				string letter;

				switch (i)
				{
					case 0:
						letter = "A";
						break;
					case 1:
						letter = "B";
						break;
					case 2:
						letter = "C";
						break;
					case 3:
						letter = "D";
						break;
					default:
						letter = "X";
						break;
				}

				GUI.color = Color.white;

				GUILayout.Label(letter, GUILayout.MaxWidth(20));

				GUILayout.EndHorizontal();

				if (_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].letter == GetLetter(i))
				{
					GUI.color = Color.green;
				}
				else
				{
					GUI.color = Color.white;
				}

				_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].answers[i] = EditorGUILayout.TextField(_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].answers[i]);

				GUI.color = Color.cyan;

				if (GUILayout.Button("✔", GUILayout.MaxWidth(20)))
				{
					_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].letter = letter;

					Debug.Log(_target.subjects[_subjectSelected].grades[_gradeSelected].questions[_questionSelected].letter);
				}
			}

			GUILayout.EndHorizontal();
		}

		GUILayout.EndVertical();

		#endregion

		#region Save & Load

		GUILayout.Space(spaceBetweenSegments);

		GUI.color = Color.white;

		GUILayout.BeginHorizontal();

		if (GUILayout.Button("SAVE"))
		{
			foreach (var item in _target.subjects)
			{
				foreach (var grades in item.grades)
				{
					foreach (var question in grades.questions)
					{
						if (string.IsNullOrEmpty(question.letter))
						{
							question.letter = "A";
						}
					}
				}
			}

			_target.SaveData(Application.dataPath + "/Resources/Data/" + "XML_Quiz.xml");

            Debug.Log("Alterações salvas.");
		}

		if (GUILayout.Button("LOAD"))
		{
            _target.subjects = QuestionManager.LoadData(Application.dataPath + "/Resources/Data/" + "XML_Quiz.xml");
            Debug.Log("Arquivo carregado.");
        }
        

        GUILayout.EndHorizontal();

		#endregion
	}

	private string GetLetter(int index)
	{
		switch (index)
		{
			case 0:
				return "A";
			case 1:
				return "B";
			case 2:
				return "C";
			case 3:
				return "D";
			default:
				return "X";
		}
	}
}