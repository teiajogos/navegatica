﻿using System.Collections.Generic;

public class Score
{
    public int SubjectID;

    public int GradeID;
    
    public int QuestionID;

    public bool IsLocked;

    public int HighScore;
}