﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsController : MonoBehaviour
{
    [Header("Screens")] //Cabeçalho do Inspector.
    public GameObject firstMovementInfo;
    public GameObject secondMovementInfo;
    public GameObject firstObstaclesInfo;
    public GameObject secondObstaclesInfo;
    public GameObject collectibleInfo;

    [Header("Arrows")] //Cabeçalho do Inspector.
    public GameObject previousScreenArrow;
    public GameObject nextScreenArrow;

    public void OnEnable()
    {
        firstMovementInfo.SetActive(true);
        firstObstaclesInfo.SetActive(false);
        secondObstaclesInfo.SetActive(false);
        collectibleInfo.SetActive(false);

        previousScreenArrow.SetActive(false);
        nextScreenArrow.SetActive(true);
    }

    public void PreviousClick()
    {
        if (secondMovementInfo.activeSelf)
        {
            secondMovementInfo.SetActive(false);
            firstMovementInfo.SetActive(true);

            previousScreenArrow.SetActive(false);
            nextScreenArrow.SetActive(true);
        }

        else if (firstObstaclesInfo.activeSelf)
        {
            firstObstaclesInfo.SetActive(false);
            secondMovementInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(true);
        }        

        else if (secondObstaclesInfo.activeSelf)
        {
            secondObstaclesInfo.SetActive(false);
            firstObstaclesInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(true);
        }

        else if (collectibleInfo.activeSelf)
        {
            collectibleInfo.SetActive(false);
            secondObstaclesInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(true);
        }
    }

    public void NextClick()
    {
        if (firstMovementInfo.activeSelf)
        {
            firstMovementInfo.SetActive(false);
            secondMovementInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(true);
        }

        else if (secondMovementInfo.activeSelf)
        {
            secondMovementInfo.SetActive(false);
            firstObstaclesInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(true);
        }

        else if (firstObstaclesInfo.activeSelf)
        {
            firstObstaclesInfo.SetActive(false);
            secondObstaclesInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(true);
        }

        else if (secondObstaclesInfo.activeSelf)
        {
            secondObstaclesInfo.SetActive(false);
            collectibleInfo.SetActive(true);

            previousScreenArrow.SetActive(true);
            nextScreenArrow.SetActive(false);
        }
    }
}
