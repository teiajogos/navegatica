﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.SceneManagement; //Utilizado para tabalhar com o sistema de cenas da Unity.
using UnityEngine.UI;

//Classe que determina como cada botão do menu principal se comporta.
public class MainMenu : MonoBehaviour
{
    public AudioController audioController; //Referência do Audio Controller do jogo.
    public SFXController sfxController;
    public GameObject playButton; //Referência do botão "Jogar".
	public GameObject storeButton; //Referência do botão "Loja".
	public GameObject optionsButton; //Referência do botão "Opções".
	public GameObject creditsButton; //Referência do botão "Créditos".
    public RawImage bigClouds;
    public RawImage smallClouds;
    public List<Subject> subjects;
    public List<Grade> grades;
    public List<Question> questions;
    public static List<Score> scores;
    public static bool loadedOnce = false;

    private void Awake()
    {
        if (!loadedOnce)
        {
            LoadLockedQuestionInformation();
        }
    }

    private void Start()
    {
        var aud = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);
        audioController.source.volume = aud;
    }

    public void Update()
    {
        bigClouds.uvRect = new Rect(bigClouds.uvRect.x + (Time.deltaTime / 15), 0, 1, 1);
        smallClouds.uvRect = new Rect(smallClouds.uvRect.x + (Time.deltaTime / 20), 0, 2, 1);
    }

    public static void LoadLockedQuestionInformation()
    {
        List<Subject> subjects = QuestionManager.LoadXMLQuiz();
        List<Grade> grades = subjects[GameController.subjectSelected].grades;
        List<Question> questions = grades[GameController.gradeSelected].questions;

        scores = new List<Score>();

        for (int subjectID = 0; subjectID < subjects.Count; subjectID++)
        {
            for (int gradeID = 0; gradeID < grades.Count; gradeID++)
            {
                for (int questionID = 0; questionID < questions.Count; questionID++)
                {
                    scores.Add(new Score()
                    {
                        SubjectID = subjectID,
                        GradeID = gradeID,
                        QuestionID = questionID,
                        IsLocked = (questionID != 0)
                    });
                }
            }
        }

        loadedOnce = true;
    }

    public void LoadScene(string scene)
	{
        var soundLength = sfxController.OnButtonPressedThroughScenes();
        StartCoroutine(WaitForButtonClick(soundLength, scene));
    }

    private IEnumerator WaitForButtonClick(float length, string sceneName)
    {
        while (length >= 0)
        {
            length -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(sceneName); 
    }

    //Ativa qualquer tela (game object) passado como parâmetro.
    public void ActivateObject(GameObject screenToActivate)
	{
		gameObject.SetActive(false); //Desativa a tela do Menu Principal.
		screenToActivate.SetActive(true); //Ativa a tela refereciada no Inspector.
    }

	//Desativa qualquer tela (game object) passado como parâmetro e ativa a do Menu Principal.
	public void BackToMenu(GameObject screenToDeactivate)
	{
		screenToDeactivate.SetActive(false); //Desativa a tela refereciada no Inspector.
		gameObject.SetActive(true); //Ativa a tela do Menu Principal.
	}
}
