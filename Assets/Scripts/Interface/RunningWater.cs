﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunningWater : MonoBehaviour {
    
    public RawImage runningWater;

    void Update ()
    {
        runningWater.uvRect = new Rect(runningWater.uvRect.x + (Time.deltaTime / 20), 0, 1, 1);
    }
}
