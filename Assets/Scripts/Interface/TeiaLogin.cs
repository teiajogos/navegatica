﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using TMPro;
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.SceneManagement; //Utilizado para tabalhar com o sistema de cenas da Unity.

// Classe da tela de login da Rede Teia.
public class TeiaLogin : MonoBehaviour
{
    public AudioController audioController; //Referência do Audio Controller do jogo.
    public string mainMenuScene; //Variável que abriga o nome da cena no menu principal.
    public string teiaLoginScene; //Variável que abriga o nome da cena no menu principal.

    [Header("Login References")]
    public TMP_InputField login;
    public TMP_InputField password;
    public GameObject loginScreen;
    public GameObject backButton;
    public GameObject guestButton;
    public GameObject loginButton;
    public GameObject incorrectPasswordText;
    public GameObject noConnectionText;
    public Sprite classicShip;
    //public GameObject loadingPanel;

    [Header("Logged User References")]
    public TextMeshProUGUI currentLoggedUser;
    public GameObject loggedUserScreen;
    public GameObject logoutPanel;
    public TextMeshProUGUI logoutWarningText;

    public static bool guestLogged = false;
    public static bool logged = false;

    [Header("Other References")] 
    public SFXController sfxController;

    private string sceneName;

    // Apagar as variáveis de teste quando o login da Teia for implementado:
    private string correctPassword;
    public static string emailInput;
    private string passwordInput;

    public void Awake()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                password.GetComponent<TMP_InputField>().placeholder.GetComponent<TextMeshProUGUI>().text = "Senha";
                break;
            case 1:
                password.GetComponent<TMP_InputField>().placeholder.GetComponent<TextMeshProUGUI>().text = "Password";
                break;
            default:
                break;
        }

        var vlm = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);

        if (!PlayerPrefs.HasKey(GameTags.PLP_MasterVolume))
        {
            PlayerPrefs.SetFloat(GameTags.PLP_MasterVolume, 100);
            audioController.source.volume = 100;
            sfxController.source.volume = 100;
        }
        else
        {
            audioController.source.volume = vlm;
            sfxController.source.volume = vlm;
        }
    }

    public void Start()
    {
        var aud = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);
        audioController.source.volume = aud;

        VerifyIfLogged();
    }
    
    public void VerifyIfLogged()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);
        
        if (!logged && !guestLogged)
        {
            loginScreen.SetActive(true);
            loggedUserScreen.SetActive(false);

            backButton.SetActive(false);
            guestButton.SetActive(true);
            loginButton.SetActive(true);
            incorrectPasswordText.SetActive(false);
        }
        else
        {
            if (guestLogged)
            {
                loginScreen.SetActive(false);
                loggedUserScreen.SetActive(true);

                switch (lng)
                {
                    case 0:
                        currentLoggedUser.text = "<b>Usuário Atual:</b> Visitante";
                        break;
                    case 1:
                        currentLoggedUser.text = "<b>Current User:</b> Guest";
                        break;
                    default:
                        break;
                }
            }

            else if (logged)
            {
                loginScreen.SetActive(false);
                loggedUserScreen.SetActive(true);

                switch (lng)
                {
                    case 0:
                        currentLoggedUser.text = "<b>Usuário Atual:</b> " + emailInput;
                        break;
                    case 1:
                        currentLoggedUser.text = "<b>Current User:</b> " + emailInput;
                        break;
                    default:
                        break;
                }
            }

            backButton.SetActive(true);
            guestButton.SetActive(false);
            loginButton.SetActive(false);
        }
    }
    
    public void UserEdit (string email)
    {
        emailInput = email;
    }
    
    public void PasswordEdit (string password)
    {
        passwordInput = password;
    }

    public void UserLogin()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            StartCoroutine(NoConnectionAvailable(2));
            return;
        }

        // Modificar ao implementar o login da Teia. Alterar os "ifs" e IDs, mas manter o resto:
        if (passwordInput == correctPassword)
        {
            logged = true;
            guestLogged = false;
            SceneManager.LoadScene("SCN_Loading");
            LoadSceneController.nextScene = mainMenuScene;
        }

        else if (passwordInput != correctPassword)
        {
            StartCoroutine(IncorrectInfo(2));
        }
    }

    //private IEnumerator LoadDuringUserLogin()
    //{
    //    loadingPanel.SetActive(true);
    //    yield return new WaitForSeconds(2);
    //    loadingPanel.SetActive(false);
    //}

    private IEnumerator IncorrectInfo(float delay)
    {
        incorrectPasswordText.SetActive(true);
        yield return new WaitForSeconds(delay);
        incorrectPasswordText.SetActive(false);
    }

    private IEnumerator NoConnectionAvailable(float delay)
    {
        noConnectionText.SetActive(true);
        yield return new WaitForSeconds(delay);
        noConnectionText.SetActive(false);
    }

    public void GuestLogin()
    {
        guestLogged = true;
        logged = false;

        LoadScene("SCN_Loading");
        LoadSceneController.nextScene = mainMenuScene;
    }

    public void Logout()
    {
        GameController.shipSelected = classicShip;
        GameController.shipSelectedIndex = 0;
        PurchaseButton.waveIndex = 0;

        ShopMenu.isBought[0] = true;
        ShopMenu.isBought[1] = false;
        ShopMenu.isBought[2] = false;
        ShopMenu.isBought[3] = false;
        ShopMenu.isBought[4] = false;

        PlayerPrefs.DeleteKey(GameTags.PLP_ShopItem);

        GameController.playerScore = 0;

        MainMenu.LoadLockedQuestionInformation();

        guestLogged = false;
        logged = false;
        logoutPanel.SetActive(false);
        LoadScene("SCN_Loading");
        LoadSceneController.nextScene = teiaLoginScene;
    }

    public void LoadScene(string name)
    {
        var soundLength = sfxController.OnButtonPressedThroughScenes();
        sceneName = name;
        StartCoroutine(WaitForButtonClick(soundLength, sceneName));
    }
    

    private IEnumerator WaitForButtonClick(float length, string sceneName)
    {
        while (length >= 0)
        {
            length -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(sceneName);
    }

    public void ActivatePanel (string panel)
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        if (panel != "logout") return;

        if (logged)
        {
            switch (lng)
            {
                case 0:
                    logoutWarningText.text = "Tem certeza?";
                    break;
                case 1:
                    logoutWarningText.text = "Are you sure?";
                    break;
                default:
                    break;
            }
        }

        else if (guestLogged)
        {
            switch (lng)
            {
                case 0:
                    logoutWarningText.text = "<b>Seu progresso será perdido.</b><br>Tem certeza?";
                    break;
                case 1:
                    logoutWarningText.text = "<b>Your progress will be lost.</b><br>Are you sure?";
                    break;
                default:
                    break;
            }
        }

        logoutPanel.SetActive(true);
    }

    public void DeactivatePanel (string panel)
    {
        if (panel == "logout")
        {
            logoutPanel.SetActive(false);
        }
    }

    public void ReturnToMenu()
    {
        LoadScene("SCN_MainMenu");
    }
}
