﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShopItem : MonoBehaviour
{
    public Sprite inGameShipSprite;
    public GameObject buttonImage;
    public int cost;
    public int index;
    public Animator animator;

    private void Update()
    {
        if (inGameShipSprite == GameController.shipSelected)
        {
            animator.SetBool("Moving", true);
        }
        else if (inGameShipSprite != GameController.shipSelected)
        {
            animator.SetBool("Moving", false);
        }
    }
}
