﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccelerometerController : MonoBehaviour
{
    public PlayerController playerController;
    public Sprite controlByClick;
    public Sprite controlByAccelerometer;

    public void Start()
    {
        OnValueChanged();
    }

    public void OnValueChanged()
    {
        if (playerController.accel.isOn) // Se estiver ativo, desabilita
        {
            PlayerPrefs.SetInt(GameTags.PLP_Accelerometer, 1);
            playerController.accel.image.sprite = controlByAccelerometer;
        }

        else if (!playerController.accel.isOn) // Se estiver inativo, habilita
        {
            PlayerPrefs.SetInt(GameTags.PLP_Accelerometer, 0);
            playerController.accel.image.sprite = controlByClick;
        }
    }
}
