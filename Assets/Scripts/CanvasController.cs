﻿using System;
using System.Collections; //Utilizado para trabalhar com IEnumerator.
using TMPro; //Utilizado para trabalhar com o plugin TextMeshPro.
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.

//Classe que define as configurações e o comportamento do Canvas durante o gameplay.
public class CanvasController : MonoBehaviour
{
	public GameController gameController; //Referência do GameController.
	public TextMeshProUGUI timerText; //Texto do temporizador.

	[HideInInspector] //Não mostra no Inspector.
	public bool isCounting; //Booleana que testa se o temporizador está rodando ou não.

	//Start é chamado no momento em que o Canvas é ativado na cena.
	private void Start()
	{
		isCounting = true; //Define que o temporizador está tocando.
		StartCoroutine(UpdateTimer()); //Corrotina que chama o atualizador do temporizador a cada 1 segundo.
	}

	//UpdateTimer roda a cada segundo.
	private IEnumerator UpdateTimer()
	{
		while (gameController.counter >= 0) //Enquanto o contador for maior que zero:
		{
			int minutes = gameController.counter / 60; //Variável armazena quantos minutos restam de acordo com gameController.counter.
			int seconds = gameController.counter % 60; //Variável armazena quantos segundos restam de acordo com gameController.counter.

			timerText.text = string.Format("{0:0}:{1:00}", minutes, seconds); //O texto do temporizador é atualizado no formato padrão de horário. {A:B} -> A é o índice (que inicia em 0) e B são quantos caracteres aparecem.

			yield return new WaitForSecondsRealtime(1); //Suspende a execução do código a seguir por 1 segundo do tempo real.

			if (isCounting) //Se o tempo está contando:
			{
				gameController.counter--; //Remove 1 de gameController.counter. 
			}
		}

		gameController.ActivateScreen(gameController.gameOver); //Ao finalizar a contagem, a tela de Game Over é chamada.
	}

    internal void UpdateTimerAfterCollision()
    {
        StopAllCoroutines();
        StartCoroutine(UpdateTimer());
    }
}
