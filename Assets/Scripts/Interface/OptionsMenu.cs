﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.
using UnityEngine.Audio; //Utilizado para trabalhar com o sistema de áudio da Unity.
using UnityEngine.UI;

//Classe que possui as funções de configuração da tela de Opções.
public class OptionsMenu : MonoBehaviour
{
    public LC_MainMenu languageController; //Referência do Audio Mixer do jogo.
    public AudioMixer audioMixer; //Referência do Audio Mixer do jogo.
    public AudioController audioController; //Referência do Audio Controller do jogo.
    public SFXController sfxController; //Referência do SFX Controller do jogo.
    public ShopMenu shopMenu; //Referência do SFX Controller do jogo.
    public Sprite classicShip;

    [Header("References")]
    public Slider volumeSlider;
    public TMP_Dropdown graphicsDropdown;
    public TMP_Dropdown languageDropdown;
    public GameObject confirmationPanel;
    public GameObject resetProgressButton;
    public GameObject progressDeletedText;

    [Header("Setting Values")]
    public static float volume;
    public static int graphicIndex;
    public static int languageIndex;

    public List<GameObject> selectedImages;
    public List<GameObject> purchaseTexts;

    public void OnEnable()
    {
        progressDeletedText.SetActive(false);
        resetProgressButton.SetActive(true);

        audioController = GameObject.FindGameObjectWithTag("AudioController").GetComponent<AudioController>();
        
        if (!PlayerPrefs.HasKey(GameTags.PLP_MasterVolume))
        {
            volumeSlider.value = 100;
        }
        else
        {
            volumeSlider.value = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);
        }

        graphicsDropdown.value = PlayerPrefs.GetInt(GameTags.PLP_GraphicSettings);
            
        languageDropdown.value = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);
    }

    //Define o volume principal do jogo de acordo com o parãmetro.
    public void SetVolume(float newVolume)
    {
        audioMixer.SetFloat(GameTags.PLP_MasterVolume, newVolume); //Define que o parâmetro "mainVolume" do Audio Mixer recebe um float equivalente á variável newVolume. 
        PlayerPrefs.SetFloat(GameTags.PLP_MasterVolume, volumeSlider.value); // O valor de volumeSlider é salvo nas PlayerPrefs.
        audioController.source.volume = volumeSlider.value;
        sfxController.source.volume = volumeSlider.value;
    }

    //Define a qualidade gráfica do jogo de acordo como o parâmetro.
    public void SetQuality(int graphicOption)
    {
        PlayerPrefs.SetInt(GameTags.PLP_GraphicSettings, graphicOption);

        switch (graphicOption)
        {
            case 0: // Baixo
                QualitySettings.SetQualityLevel(0);
                break;
            case 1: // Médio
                QualitySettings.SetQualityLevel(2);
                break;
            case 2: // Alto
                QualitySettings.SetQualityLevel(5);
                break;
            default:
                break;
        }

        graphicIndex = graphicsDropdown.value;
    }

    public void SetLanguage (int languageOption)
    {
        PlayerPrefs.SetInt(GameTags.PLP_LanguageSettings, languageOption);
        languageController.OnLanguageChanged(languageOption);
    }

    public void ActivatePanel()
    {
        confirmationPanel.SetActive(true);
    }

    public void DeactivatePanel()
    {
        confirmationPanel.SetActive(false);
    }

    public void ResetProgress()
    {
        GameController.shipSelected = classicShip;
        GameController.shipSelectedIndex = 0;
        PurchaseButton.waveIndex = 0;

        foreach (var image in selectedImages)
        {
            image.SetActive(false);
        }

        foreach (var text in purchaseTexts)
        {
            text.SetActive(true);
        }

        ShopMenu.isBought[0] = true;
        ShopMenu.isBought[1] = false;
        ShopMenu.isBought[2] = false;
        ShopMenu.isBought[3] = false;
        ShopMenu.isBought[4] = false;
        shopMenu.SaveBoughtShipInfo();

        PlayerPrefs.DeleteKey(GameTags.PLP_ShopItem);

        GameController.playerScore = 0;

        MainMenu.LoadLockedQuestionInformation();

        confirmationPanel.SetActive(false);
        
        resetProgressButton.SetActive(false);
        progressDeletedText.SetActive(true);
    }
}
