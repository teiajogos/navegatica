﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource source;

    public AudioClip menuSoundtrack;
    
    public void Awake()
    {
        var currentSource = GameObject.FindGameObjectsWithTag("AudioController");

        if (currentSource != null && currentSource.Length > 1)
        {
            Destroy(this.gameObject);
        }

        source.volume = PlayerPrefs.GetFloat(GameTags.PLP_MasterVolume);
        source.Play();

        DontDestroyOnLoad(this.gameObject);
    }

    public void OnGameStart()
    {
        Destroy(this.gameObject);
    }
}
