﻿using UnityEngine; //Utilizado para trabalhar com o MonoBehaviour e as classes e funções da Unity.

//Classe que serve simplesmente para armazenar variável de na doca (utiliza o MonoBehaviour para aparecer no Inspector).
public class DockAnswer : MonoBehaviour
{
	public string answer; //Variável que abriga a resposta referente à doca.
}