﻿using System.Linq;
using TMPro;
using UnityEngine;

public class ShopMenu : MonoBehaviour
{
	public TextMeshProUGUI pointsText;
	public static string buyText;
	public static string selectText;
    
    public static bool[] isBought = new bool[5];

    private void Awake()
    {
        isBought[0] = true;

        if (string.IsNullOrEmpty(PlayerPrefs.GetString(GameTags.PLP_ShopItem)))
        {
            SaveBoughtShipInfo();
        }

        GetBoughtShipInfo();
    }

    private void OnEnable()
    {
        var lng = PlayerPrefs.GetInt(GameTags.PLP_LanguageSettings);

        switch (lng)
        {
            case 0:
                pointsText.text = "SEUS PONTOS: " + GameController.playerScore; // Português
                break;
            case 1:
                pointsText.text = "YOUR POINTS: " + GameController.playerScore; // Inglês
                break;
            default:
                break;
        }
    }

    public void SaveBoughtShipInfo()
    {
        string shipArray = isBought.Aggregate(string.Empty, (current, item) => current + (item + ";"));

        PlayerPrefs.SetString(GameTags.PLP_ShopItem, shipArray);
        PlayerPrefs.Save();
    }
    
    public void GetBoughtShipInfo()
    {
        var shipString = PlayerPrefs.GetString(GameTags.PLP_ShopItem);
    
        var shipArray = shipString.Split(';');

        for (int i = 0; i < 5; i++)
        {
            isBought[i] = (shipArray[i] == "True");
        }
    }
}