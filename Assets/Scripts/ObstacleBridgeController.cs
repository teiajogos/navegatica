﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBridgeController : MonoBehaviour {

    public GameObject obstacleBridge;
    private GameObject instance;
    private Vector3 spawn;

    public void InstantiateBridge(Vector2 emptyColumnPos)
    {
        spawn = new Vector3(emptyColumnPos.x, emptyColumnPos.y, 0);
        instance = Instantiate(obstacleBridge, spawn, new Quaternion(0, 0, 0, 0));
    }

    //private void Update()
    //{
    //    if (instance != null)
    //    {
    //        instance.transform.position = spawn;
    //    }
    //}
}
