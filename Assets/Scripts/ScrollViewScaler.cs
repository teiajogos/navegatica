﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewScaler : MonoBehaviour
{
	public GameObject grid;

	void Start()
	{
		RectTransform rectTransform = GetComponent<RectTransform>();

		float spacing = GetComponent<GridLayoutGroup>().spacing.y; //Armazena o espaçamento do GridLayoutGroup do objeto atual na variável.
		float cellSize = GetComponent<GridLayoutGroup>().cellSize.y;
		float height = GetComponent<RectTransform>().rect.height;
		int constraintCount = GetComponent<GridLayoutGroup>().constraintCount;
		int childCount = transform.childCount;

		int missingCells = constraintCount - (childCount % constraintCount); //Faz o cálculo de quantos blocos restam para fechar a grade perfeita.
		int numCellsPerColumn = (childCount + missingCells) / constraintCount; //Número de células por coluna.

		float width = grid.GetComponent<RectTransform>().rect.x;
		height = numCellsPerColumn * cellSize + spacing * (numCellsPerColumn - 1);

		rectTransform.sizeDelta = new Vector2(width, height);
	}
}
