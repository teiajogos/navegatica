﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using UnityEngine.Events;

//using UnityEditor; 

/// <summary>
/// 
/// VERSAO STAND_ALONE
/// 
/// Criando um XML
/// 
/// Os dados precisam estar em forma de ums string, para isso, utilize o metodo XMLSerialize
/// 
/// var Dados_Serializados = Nome_Do_Objeto.XmlSerialize();
/// 
/// XmlHelper.CreateXML(Local_do_arquivo, Nome_do_arquivo, Dados_Serializados);
/// 
/// 
/// Carregando um XML
/// 
/// Crie uma string vazia e chame o metodo LoadXML. A string precisa ser criada fora do metodo, pois ela sera utilizada em seguida
/// 
/// var string_referencia = String.Empty;
/// XmlHelper.LoadXML(Local_do_arquivo+Nome_do_arquivo, ref string_referencia);
/// 
/// Eh sempre bom verificar se o arquivo carregado nao gerou uma string nula ou vazia. Apos o teste, deserialize a string para o tipo do objeto que voce quer.
/// 
/// if (!String.IsNullOrEmpty(string_referencia)){
/// 	Nome_Do_Objeto = string_referencia.XmlDeserialize<TIPO_DO_OBJETO>();
/// }
/// 
/// VERSAO WEB EM BREVE!!!
/// 
/// </summary>

namespace V_Lib
{	
	public static class StringExtensions
	{
        public static string XmlSerialize<T>(this T obj) where T : class, new()
		{
			if (obj == null)
				return "null object";
			
			XmlSerializer s = new XmlSerializer(typeof(T));
			StringWriter w = new StringWriter();

			s.Serialize(w, obj);
			w.Close();

			return w.ToString();
		}
		public static T XmlDeserialize<T>(this string xml) where T : class, new()
		{
			if (xml == null)
				return null;
			
			XmlSerializer s = new XmlSerializer(typeof(T));
			StringReader r = new StringReader(xml);
			
			try 
			{ 
				T obj = (T) s.Deserialize(r);
				r.Close();
				return obj;
			}
			catch (Exception e)
			{
				Debug.Log (e);
				return null;
			}
		}
        public static string FromCurrencyString(this string texto)
        {
            texto = texto.Replace("$", "");
            texto = texto.Replace(".", "");
            texto = texto.Replace(",", ".");

            return texto;
        }
        public static string FromCurrencyStringToInt(this string texto)
        {
            if (texto.Contains("$"))
            {
                texto = texto.Replace("$", "");
                texto = texto.Replace(",", "");

                texto = texto.Replace(".", "");

                if (texto.Length - 2 > 0)
                    texto = texto.Substring(0, texto.Length - 2);
            }
            return texto;
        }
        public static string FromPercentString(this string texto)
        {
            texto = texto.Replace("%", "");
            //texto = texto.Replace(".", "");
            //texto = texto.Replace(",", ".");

            return texto;
        }
        public static string[] ReadXmlString(this string myXml)
        {
            if (string.IsNullOrEmpty(myXml))
                return null;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(myXml);

            var resposta = new string[xmlDoc.DocumentElement.ChildNodes.Count];

            for (int i = 0; i < resposta.Length; i++)
            {
                resposta[i] = xmlDoc.DocumentElement.ChildNodes[i].InnerText;
            }

            return resposta;       
        }
        public static string Encrypt(this string toEncrypt, string key = null)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(string.IsNullOrEmpty(key) ? "3E7D3E36F80E4480A2B621A9D8AC4E96" : key);
            // 256-AES key
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7;
            // better lang support
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string Decrypt(this string toDecrypt, string key = null)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(string.IsNullOrEmpty(key) ? "3E7D3E36F80E4480A2B621A9D8AC4E96" : key);
            // AES-256 key
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7;
            // better lang support
            ICryptoTransform cTransform = rDel.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public static bool IsValidEmail(this string strIn)
        {
            var invalid = false;

            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", V_Lib.StringExtensions.DomainMapper, RegexOptions.None);
            }
            catch
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase);
            }
            catch
            {
                return false;
            }
        }
        private static string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            
            domainName = idn.GetAscii(domainName);
            
            return match.Groups[1].Value + domainName;
        }
	}
    public static class MathfDouble
    {
        /// <param name="t">Time</param>
        /// <param name="pA">Point B</param>
        /// <param name="hA">Handler A</param>
        /// <param name="hB">Handler B</param>
        /// <param name="pB">Point B</param>
        public static Vector3 CalculateBezierPoint(float t, Vector3 pA, Vector3 hA, Vector3 hB, Vector3 pB)
        {
            float u = 1 - t;
            float tt = t * t;
            float uu = u * u;
            float uuu = uu * u;
            float ttt = tt * t;

            Vector3 p = uuu * pA;   //first term
            p += 3 * uu * t * hA;   //second term
            p += 3 * u * tt * hB;   //third term
            p += ttt * pB;          //fourth term

            return p;
        }
        public static float ClearFloat(this float value, int casas = 1)
        {
            return value = ((int)(value * 10 * casas)) / (10F * casas);
        }
        public static float ShortFloat(this float value, int multiplier = 1)
        {
            multiplier *= 10;

            var higherFloat = value * multiplier;

            return (higherFloat - (higherFloat - (int)higherFloat)) / multiplier;
        }
    }
    public static class RendererExtensions
    {
        public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
            return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
        }
    }
    public static class ParticleSystemExtensions
    {
        public static void SetEmissionRate(this ParticleSystem particleSystem, float emissionRate)
        {
            var emission = particleSystem.emission;
            emission.rateOverTime = new ParticleSystem.MinMaxCurve(emissionRate);
        }
    }
    public static class ColliderExtensions
    {
        public static bool IsVisibleFrom(this Collider collider, Camera camera)
        {
            Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
            return GeometryUtility.TestPlanesAABB(planes, collider.bounds);
        }
    }
    public static class GameObjectExtensions
    {
        public static T New<T>(string name, Transform parent = null)
        {
            var go = new GameObject(name, new Type[] { typeof(T) });

            go.SetParentAndZero(parent);

            return go.GetComponent<T>();
        }
        public static void SetLayerMask(this GameObject go, string layerName)
        {
            int layerID = LayerMask.NameToLayer(layerName);

            SetLayerMask(go, layerID);
        }
        public static void SetLayerMask (this GameObject go, int layerID)
        {
            foreach (var item in go.GetComponentsInChildren<Transform>())
            {
                item.gameObject.layer = layerID;
            }
        }
        public static void SetParentAndZero(this GameObject go, Transform parent)
        {
            go.transform.SetParent(parent);

            go.transform.localPosition = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;
        }
    }
    public static class ButtonExtensions
    {
        public static void SetAction(this Button bt, UnityAction action)
        {
            if (bt.onClick == null)
                bt.onClick = new Button.ButtonClickedEvent();

            bt.onClick.AddListener(action);
        }
    }
    public class XmlReturn
    {
        public bool error;
        public string Message;

        public XmlReturn(string Message, bool error = false)
        {
            this.error = error;
            this.Message = Message;
        }
    }

    public class XmlHelper : MonoBehaviour
    {
        public static bool Busy {get; private set;}

        private static XmlReturn CreateXmlLocal(string fileLocation, string fileName, string data)
        {
#if !UNITY_WEBPLAYER
            Busy = true;

            try
            {
                StreamWriter w;

                if (!Directory.Exists(fileLocation))
                    Directory.CreateDirectory(fileLocation);

                FileInfo f = new FileInfo(fileLocation + fileName);

                if (f.Exists)
                {
                    try
                    {
                        f.Delete();
                    }
                    catch (IOException e)
                    {
                        Busy = false;
                        return new XmlReturn("Erro ao tentar deletar o arquivo: " + e.Message, true);
                    }
                }

                w = f.CreateText();
                w.Write(data);
                w.Close();

                Busy = false;

                //Debug.Log("Xml Created at " + f.Directory + "\\" + fileName);
                return new XmlReturn("Xml Created at " + f.Directory + "\\" + fileName);
            }
            catch (System.Exception e)
            {
                Busy = false;
                return new XmlReturn(e.Message, true);
            }
#else
            return new XmlReturn("Não é possível salvar no modo webplayer", true);
#endif

        }
        public static XmlReturn CreateXML(string fileLocation, string fileName, string data) 
		{ 
			return CreateXmlLocal(fileLocation, fileName, data);
		}
        public static string LoadXML(string fileLocation)
        {
            FileInfo f = new FileInfo(fileLocation);

            if (!f.Exists)
            {
                Busy = false;
                throw new IOException("Arquivo não encontrado");
            }

            StreamReader r = File.OpenText(fileLocation);
            string info = r.ReadToEnd();
            r.Close();

            //Debug.Log("Xml Loaded from " + fileLocation);

            return info;
        }
        public static bool LoadXML(string fileLocation, ref string data) 
		{ 
			FileInfo f = new FileInfo(fileLocation);

            if (!f.Exists){
				Busy = false;
				return false;
			}
			
			StreamReader r = File.OpenText(fileLocation);
			string info = r.ReadToEnd(); 
			r.Close(); 
			data = info; 
			
			Debug.Log("Xml Loaded from "+fileLocation);

			return true;
		} 
    }
}